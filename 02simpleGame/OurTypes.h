#ifndef OURTYPES_H
#define OURTYPES_H
#define _CRT_SECURE_NO_WARNINGS

#include "ebentoak.h"

typedef enum { JOLASTEN, GALDU, IRABAZI }EGOERA;
typedef enum { IRUDIA, MARGOA, TESTUA } MOTA;

typedef struct
{
  POSIZIOA pos;
  int id;
  MOTA mota;
}JOKO_ELEMENTUA;

struct DATUAK {
	int score;
	int bizitzak;
	int zenbatPantailan;
	int zenbatTiro;
	int zailtasuna;
	int tiroMaximoak;
	int tiroa;
	int babesa;
	int tiroAbilitatea;
	int abiaduraMoteldu;
}datuak;

#endif