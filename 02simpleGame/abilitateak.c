#include "abilitateak.h"
#include "OurTypes.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "SDL_ttf.h"
#include "imagen.h"
#include "text.h"
#include "tiroak.h"
#include "asteroideak.h"



int babes_nibela = 1, abiadura_nibela = 1;
double tiro_nibela = 1, tiro_nibel_biderkatzaile = 0;
void abilitateak(int idAbiadura,int idBabesa, int idTiroak) {
	
	char babesa[10], tiroak[10], abiadura[10];
	irudiaMugitu(idAbiadura, 200, 0);
	irudiakMarraztu();
	irudiaMugitu(idTiroak, 300, 0);
	irudiakMarraztu();
	irudiaMugitu(idBabesa, 400, 0);
	irudiakMarraztu();

	if (datuak.score > 1000 * babes_nibela) {
		datuak.babesa++;
		babes_nibela++;
	}
	if (datuak.score > 750 * abiadura_nibela) {
		datuak.abiaduraMoteldu++;
		abiadura_nibela++;
	}
	if (datuak.score > 600 * tiro_nibela) {
		datuak.tiroAbilitatea = datuak.tiroAbilitatea + 5;
		tiro_nibela = tiro_nibela + 1.5* tiro_nibel_biderkatzaile;
		tiro_nibel_biderkatzaile++;
	}
	sprintf(babesa, "%d", datuak.babesa);
	sprintf(tiroak, "%d", datuak.tiroAbilitatea);
	sprintf(abiadura, "%d", datuak.abiaduraMoteldu);
	textuaIdatzi(170, 10, abiadura);
	textuaIdatzi(270, 10, tiroak);
	textuaIdatzi(370, 10, babesa);
	
}
void abilitateTiroa(int x, int y) {
	for (int j = 0; j < 12; j++) {
		tiro[datuak.tiroa].x1 = x + (-10 * sin(12 * j));
		tiro[datuak.tiroa].y1 = y - (10 * cos(12 * j));
		tiro[datuak.tiroa].x2 = x + (-20 * sin(12 * j));
		tiro[datuak.tiroa].y2 = y - (20 * cos(12 * j));
		tiro[datuak.tiroa].vx = 0 + (-10 * sin(12 * j));
		tiro[datuak.tiroa].vy = 3 - (10 * cos(12 * j));
		datuak.zenbatTiro++;
		datuak.tiroa++;
		

	}
}
void abilitateAsteroideenAbiaduraMoteldu() {
	for (int i = 0; i < datuak.zenbatPantailan; i++) {
		asteroide[i].vx = asteroide[i].vx *0.5;
		asteroide[i].vy = asteroide[i].vy *0.5;
	}
}