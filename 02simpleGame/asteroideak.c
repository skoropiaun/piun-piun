#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "SDL_ttf.h"
#include "graphics.h"
#include"konstanteak.h"
#include "OurTypes.h"
#include "asteroideak.h"

//ASTEROIDEEN PARAMETROAK KALKULATU. ABIADURA ETA ALDE BATETIK BESTERA PASA.
void asteroideaMugitu() {
	int j = 0;
	for (int i = 0; i < datuak.zenbatPantailan; i++) {
		asteroide[i].x = asteroide[i].x + asteroide[i].vx;
		asteroide[i].y = asteroide[i].y + asteroide[i].vy;
		for (j = 0; j < 12; j++) {
			asteroide[i].puntua[j].x = asteroide[i].puntua[j].x + asteroide[i].vx;
			asteroide[i].puntua[j].y = asteroide[i].puntua[j].y + asteroide[i].vy;
		}
		//  X ARDATZEAN ALDE BATETIK BESTERA
		if (asteroide[i].x > 633) {
			asteroide[i].x = asteroide[i].x + asteroide[i].radio - 633;
			for (j = 0; j < 12; j++) {
				asteroide[i].puntua[j].x = asteroide[i].puntua[j].x + asteroide[i].radio - 633;
			}
		}
		else if (asteroide[i].x < 7) {
			asteroide[i].x = asteroide[i].x - asteroide[i].radio + 633;
			for (j = 0; j < 12; j++) {
				asteroide[i].puntua[j].x = asteroide[i].puntua[j].x - asteroide[i].radio + 633;
			}
		}
		//  Y ARDATZEAN ALDE BATETIK BESTERA
		if (asteroide[i].y > 480) {
			asteroide[i].y = asteroide[i].y + asteroide[i].radio - 430;
			for (j = 0; j < 12; j++) {
				asteroide[i].puntua[j].y = asteroide[i].puntua[j].y + asteroide[i].radio - 430;
			}
		}
		else if (asteroide[i].y < 50) {
			asteroide[i].y = asteroide[i].y - asteroide[i].radio + 430;
			for (j = 0; j < 12; j++) {
				asteroide[i].puntua[j].y = asteroide[i].puntua[j].y - asteroide[i].radio + 430;
			}
		}
	}
}
// ASTEROIDEAK MARRAZTU. ETA BERE TAMAINAREN ARABERA ARKATZ KOLOREA EZARRI
void asteroideaMarraztu() {

	int i = 0, y_berri = 0, j = 0;
	for (i = 0; i < datuak.zenbatPantailan; i++) {
		switch (asteroide[i].tipo) {
		case 0: arkatzKoloreaEzarri(245, 100, 150);
			break;
		case 1: arkatzKoloreaEzarri(190, 190, 150);
			break;
		case 2: arkatzKoloreaEzarri(0, 255, 0);
			break;
		case 3: arkatzKoloreaEzarri(255, 0, 0);
			break;
		}
		//ASTEROIDEAK SORTZEKO LINEAK MARRAZTU
		for (j = 0; j < 11; j++) {
			zuzenaMarraztu((int)asteroide[i].puntua[j].x, (int)asteroide[i].puntua[j].y, (int)asteroide[i].puntua[j + 1].x, (int)asteroide[i].puntua[j + 1].y);
		}
		zuzenaMarraztu((int)asteroide[i].puntua[11].x, (int)asteroide[i].puntua[11].y, (int)asteroide[i].puntua[0].x, (int)asteroide[i].puntua[0].y);

	}

}

//ASTEROIDEA PUSKATZEN DENEAN ASTEROIDE BERRIAK KALKULATU. TAMAINAREN ARABERA: TXIKIENA CASE 0, ERTAINTXIKIA 1, ERTAINHANDIA 2 ETA HANDIA 3.

void asteroideBerriaKalkulatu(int tipo, int x, int y) {

	asteroide[ASTEROIDE_ZENBAKIA].x = x;
	asteroide[ASTEROIDE_ZENBAKIA].y = y;
	do {
		asteroide[ASTEROIDE_ZENBAKIA].vx = -2 + rand() % ((2 + 1) + 2);
		asteroide[ASTEROIDE_ZENBAKIA].vy = -2 + rand() % ((2 + 1) + 2);
	} while (asteroide[ASTEROIDE_ZENBAKIA].vx == 0 || asteroide[ASTEROIDE_ZENBAKIA].vy == 0);

	switch (tipo) {
	case 0: datuak.zenbatPantailan--;
		break;
	case 1: asteroide[ASTEROIDE_ZENBAKIA].radio = 6;
		asteroide[ASTEROIDE_ZENBAKIA].tipo = 0;
		for (int j = 0; j < 12; j++) {
			int y_berri = asteroide[ASTEROIDE_ZENBAKIA].radio + (-1 + rand() % ((1 + 1) + 1));
			asteroide[ASTEROIDE_ZENBAKIA].puntua[j].x = asteroide[ASTEROIDE_ZENBAKIA].x + (-y_berri * sin(12 * j));
			asteroide[ASTEROIDE_ZENBAKIA].puntua[j].y = asteroide[ASTEROIDE_ZENBAKIA].y - (+y_berri * cos(12 * j));
		}
		break;
	case 2: asteroide[ASTEROIDE_ZENBAKIA].radio = 10;
		asteroide[ASTEROIDE_ZENBAKIA].tipo = 1;
		for (int j = 0; j < 12; j++) {
			int y_berri = asteroide[ASTEROIDE_ZENBAKIA].radio + (-3 + rand() % ((3 + 1) + 3));
			asteroide[ASTEROIDE_ZENBAKIA].puntua[j].x = asteroide[ASTEROIDE_ZENBAKIA].x + (-y_berri * sin(12 * j));
			asteroide[ASTEROIDE_ZENBAKIA].puntua[j].y = asteroide[ASTEROIDE_ZENBAKIA].y - (+y_berri * cos(12 * j));
		}
		break;
	case 3: asteroide[ASTEROIDE_ZENBAKIA].radio = 18;
		asteroide[ASTEROIDE_ZENBAKIA].tipo = 2;
		for (int j = 0; j < 12; j++) {
			int y_berri = asteroide[ASTEROIDE_ZENBAKIA].radio + (-5 + rand() % ((5 + 1) + 5));
			asteroide[ASTEROIDE_ZENBAKIA].puntua[j].x = asteroide[ASTEROIDE_ZENBAKIA].x + (-y_berri * sin(12 * j));
			asteroide[ASTEROIDE_ZENBAKIA].puntua[j].y = asteroide[ASTEROIDE_ZENBAKIA].y - (+y_berri * cos(12 * j));
		}
		break;
	}
}

// ASTEROIDE HANDIA SORTZEKO.

void puntuenAraberaAsteroideaSortu() {

	if (datuak.score > 450 * datuak.zailtasuna) {
		datuak.zenbatPantailan++;
		asteroide[ASTEROIDE_ZENBAKIA].x = 1 + rand() % ((640 + 1) - 1);
		asteroide[ASTEROIDE_ZENBAKIA].y = 1 + rand() % ((480 + 1) - 1);
		asteroide[ASTEROIDE_ZENBAKIA].tipo = 3;
		asteroide[ASTEROIDE_ZENBAKIA].radio = 30;
		do {
			asteroide[ASTEROIDE_ZENBAKIA].vx = -2 + rand() % ((2 + 1) + 2);
			asteroide[ASTEROIDE_ZENBAKIA].vy = -2 + rand() % ((2 + 1) + 2);
		} while (asteroide[ASTEROIDE_ZENBAKIA].vx == 0 || asteroide[ASTEROIDE_ZENBAKIA].vy == 0);
			
		for (int j = 0; j < 12; j++) {
			int y_berri = asteroide[ASTEROIDE_ZENBAKIA].radio + (-7 + rand() % ((7 + 1) + 7));
			asteroide[ASTEROIDE_ZENBAKIA].puntua[j].x = asteroide[ASTEROIDE_ZENBAKIA].x + (-y_berri * sin(12 * j));
			asteroide[ASTEROIDE_ZENBAKIA].puntua[j].y = asteroide[ASTEROIDE_ZENBAKIA].y - (+y_berri * cos(12 * j));
		}
		datuak.zailtasuna++;
	}
}