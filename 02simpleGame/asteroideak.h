#include "SDL.h"
struct ASTEROIDE {
	double x;
	double y;
	int radio;
	double vx;
	double vy;
	int tipo;
	struct PUNTOAK {
		double x;
		double y;
	}puntua[12];
}asteroide[100];

void asteroideaMugitu();
void asteroideaMarraztu();
void asteroideBerriaKalkulatu(int tipo, int x, int y);
void puntuenAraberaAsteroideaSortu();