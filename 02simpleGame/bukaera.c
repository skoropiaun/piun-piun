#include "jokoa.h"
#include "imagen.h"
#include "graphics.h"
#include "ebentoak.h"
#include "text.h"
#include "soinua.h"
#include <stdio.h>
#include <windows.h>
#include "konstanteak.h"
#include "bukaera.h"
#include "OurTypes.h"



#define _CRT_SECURE_NO_WARNINGS
#define ONGI_ETORRI_MEZUA "Sakatu return hasteko..."
#define JOKOA_BIZITZAK ".\\img\\bizitza.bmp"
#define BUKAERA_SOUND_1 ".\\sound\\128NIGHT_01.wav"
#define HASIBERRIRO "BERRIRO JOLASTU"
#define EXIT "EXIT"
#define FLECHA ".\\img\\flecha .bmp"
#define FONDO_BUKAERAAMAIERA ".\\img\\BUKAERA.bmp"
#define BERRIRO_JOLASTU ".\\img\\MENU.bmp"
#define EXIT_AMAIERA ".\\img\\EXIT.bmp"
#define MENU_SOINUA ".\\sound\\misc_menu_3.wav"
#define SA_MATAO_PACO ".\\sound\\paco.wav"
#define SCORE "Puntuazioa :"




int  jokoAmaierakoa(EGOERA egoera)
{

	int ebentu = 0, idFondokoIrudia, idFlechaMenu, idFlechaExit, arratoiarePosizioa = 0;
	int idMenuSoinua = loadSound(MENU_SOINUA);
	int idAudioGame;
	idFlechaExit = FLECHA_Exit();
	irudiaKendu(idFlechaExit);
	loadTheMusic(BUKAERA_SOUND_1);



	idAudioGame = loadSound(SA_MATAO_PACO);
	playSound(idAudioGame);
	idFlechaMenu = FLECHA_BerriroJolastu();
	irudiaKendu(idFlechaMenu);
	
	

	char puntuazioa[10];
	sprintf(puntuazioa, "%d", datuak.score);
	idFondokoIrudia = BUKAERAKO_irudiaBistaratu();
	idFlechaMenu = FLECHA_BerriroJolastu();
	pantailaBerriztu();


	do
	{
		textuaIdatzi(320, 143, puntuazioa);
		ebentu = ebentuaJasoGertatuBada();
		amaierakoTestua(arratoiarePosizioa);
		
		

		if (ebentu == TECLA_DOWN && arratoiarePosizioa == 0)
		{
			playSound(idMenuSoinua);
			arratoiarePosizioa = 1;
			irudiaKendu(idFlechaMenu);
			idFlechaExit = FLECHA_Exit();
		}
		else if (ebentu == TECLA_UP && arratoiarePosizioa == 1)
		{
			playSound(idMenuSoinua);
			arratoiarePosizioa = 0;
			irudiaKendu(idFlechaExit);
			idFlechaMenu = FLECHA_BerriroJolastu();
		}


	} while ((ebentu != TECLA_RETURN) && (ebentu != SAGU_BOTOIA_ESKUMA));
	irudiaKendu(idFlechaMenu);
	if (arratoiarePosizioa == 1)
	{
		sgItxi();
	}
	else
	{
		egoera = 1;
	}
	audioTerminate();
	irudiaKendu(idFondokoIrudia);
	irudiaKendu(idFlechaMenu);




	return (ebentu != TECLA_RETURN) ? 1 : 0;

}
//MENU PUNTUAKETA ETA EXIT TESTUAK IDATZI
void amaierakoTestua(int posizioa)
{
	textuaIdatzi(220, 142, SCORE);
	if (posizioa == 0)
	{
		textuaAutatu(230, 300, HASIBERRIRO);
		textuaIdatzi(280, 400, EXIT);
	}
	if (posizioa == 1)
	{
		textuaIdatzi(230, 300, HASIBERRIRO);
		textuaAutatu(280, 400, EXIT);
	}


	pantailaBerriztu();
}
//IRUDIAK JARRI
int FLECHA_BerriroJolastu() {
	int id3 = -1;
	pantailaGarbitu();
	id3 = irudiaKargatu(FLECHA);
	irudiaMugitu(id3, 130, 285);
	irudiakMarraztu();
	pantailaBerriztu();
	return id3;
}
int FLECHA_Exit() {
	int id3 = -1;
	pantailaGarbitu();
	id3 = irudiaKargatu(FLECHA);
	irudiaMugitu(id3, 170, 380);
	irudiakMarraztu();
	pantailaBerriztu();
	return id3;
}
int IMAGE_MENU() {
	int id7 = -1;
	pantailaGarbitu();
	id7 = irudiaKargatu(BERRIRO_JOLASTU);
	irudiaMugitu(id7, 170, 380);
	irudiakMarraztu();
	pantailaBerriztu();
	return id7;
}
int IMAGE_EXIT() {
	int id8 = -1;
	pantailaGarbitu();
	id8 = irudiaKargatu(EXIT_AMAIERA);
	irudiaMugitu(id8, 170, 380);
	irudiakMarraztu();
	pantailaBerriztu();
	return id8;
}
int BUKAERAKO_irudiaBistaratu() {
	int id6 = -1;
	id6 = irudiaKargatu(FONDO_BUKAERAAMAIERA);
	irudiaMugitu(id6, 0, 0);
	irudiakMarraztu();
	pantailaBerriztu();
	return id6;
}