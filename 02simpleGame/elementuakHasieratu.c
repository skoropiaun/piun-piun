#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "SDL_ttf.h"
#include "elementuakHasieratu.h"
#include"konstanteak.h"
#include "OurTypes.h"
#include "ontzia.h"
#include "asteroideak.h"

// DATUAK HASIERATU
void datuakHasieratu() {
	datuak.bizitzak = 3;
	datuak.score = 0;
	datuak.zenbatPantailan = 1;
	datuak.zailtasuna = 1;
	datuak.tiroMaximoak = 50;
	datuak.zenbatTiro = 0;
	datuak.tiroa = 0;
	datuak.abiaduraMoteldu = 0;
	datuak.babesa = 0;
	datuak.tiroAbilitatea = 5;
}
void ontziaHasieratu() {
	ontzia.a = 0;
	ontzia.y = 240;
	ontzia.x = 320;
	ontzia.dx = 10;
	ontzia.dy = 10;
	ontzia.vy = 0;
	ontzia.vx = 0;
	ontzia.ax = 0;
	ontzia.ay = 0.2;
}
//ASTEROIDEEN POSIZIOA ETA ABIADURA KALKULATU
void asteroideakHasieratu() {

	int i = 0;
	while (i < datuak.zenbatPantailan) {
		asteroide[i].x = 1 + rand() % ((640 + 1) - 1);
		asteroide[i].y = 1 + rand() % ((480 + 1) - 1);
		asteroide[i].tipo = 3;
		asteroide[i].radio = 30;
		do {
			asteroide[i].vx = -2 + rand() % ((2 + 1) + 2);
			asteroide[i].vy = -2 + rand() % ((2 + 1) + 2);
		} while (asteroide[i].vx == 0 || asteroide[i].vy == 0);

		for (int j = 0; j < 12; j++) {
			int y_berri = asteroide[i].radio + (-7 + rand() % ((7 + 1) + 7));
			asteroide[i].puntua[j].x = asteroide[i].x + (-y_berri * sin(12 * j));
			asteroide[i].puntua[j].y = asteroide[i].y - (+y_berri * cos(12 * j));
		}
		i++;
	}
}