#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "SDL_ttf.h"

#include "imagen.h"
#include "graphics.h"
#include "funtzioak.h"
#include"konstanteak.h"
#include "OurTypes.h"



void ontziaHasieratu() {
	ontzia.a = 0;
	ontzia.y = 240;
	ontzia.x = 320;
	ontzia.dx = 10;
	ontzia.dy = 10;
	ontzia.vy = 0;
	ontzia.vx = 0;
	ontzia.ax = 0;
	ontzia.ay = 0.2;
}
void datuakHasieratu() {
	datuak.score = 0;
	datuak.bizitzak = 3;
	datuak.zenbatAsteroidePantailan = 2;
}
void ontziaMarraztu(int x, int y, int a) {

	float x_berri1 = x + (-10 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri1 = y - (10 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri2 = x + (+7.07106781*cos(a*ROTAZIOA_MURRIZTU) + 7.07106781*sin(a*ROTAZIOA_MURRIZTU));
	float y_berri2 = y - (7.07106781*sin(a*ROTAZIOA_MURRIZTU) - 7.07106781*cos(a*ROTAZIOA_MURRIZTU));
	float x_berri3 = x + (-7.07106781*cos(a*ROTAZIOA_MURRIZTU) + 7.07106781*sin(a*ROTAZIOA_MURRIZTU));
	float y_berri3 = y - (-7.07106781*sin(a*ROTAZIOA_MURRIZTU) - 7.07106781*cos(a*ROTAZIOA_MURRIZTU));

	arkatzKoloreaEzarri(0, 245, 230);
	zuzenaMarraztu(x_berri1, y_berri1, x_berri2, y_berri2);
	zuzenaMarraztu(x_berri2, y_berri2, x_berri3, y_berri3);
	zuzenaMarraztu(x_berri3, y_berri3, x_berri1, y_berri1);
}


/*void asteroideaKalkulatu() {
	srand(time(NULL));
	int i = 0;
	while(i<datuak.zenbatAsteroidePantailan) {
		asteroide[i].x = 1 + rand() % ((640 + 1) - 1);
		asteroide[i].y = 1 + rand() % ((480 + 1) - 1);
		asteroide[i].tipo = 2;
		if (asteroide[i].tipo == 0)asteroide[i].radio = 6;
		else if (asteroide[i].tipo == 1)asteroide[i].radio = 10;
		else if (asteroide[i].tipo == 2)asteroide[i].radio = 18;
		else if (asteroide[i].tipo == 3)asteroide[i].radio = 30;

		while (asteroide[i].vx == 0 || asteroide[i].vy == 0) {
			asteroide[i].vx = -2 + rand() % ((2 + 1) + 2);
			asteroide[i].vy = -2 + rand() % ((2 + 1) + 2);
		}
		i++;
	}
}*/
void asteroideaMugitu() {
	for (int i = 0; i < datuak.zenbatAsteroidePantailan; i++) {
		asteroide[i].x = asteroide[i].x + asteroide[i].vx;
		asteroide[i].y = asteroide[i].y + asteroide[i].vy;
		if (asteroide[i].x > 640) {
			asteroide[i].x = asteroide[i].x - 640;
		}
		else if (asteroide[i].x < 0) {
			asteroide[i].x = asteroide[i].x + 640;
		}
		if (asteroide[i].y > 480) {
			asteroide[i].y = asteroide[i].y - 480;
		}
		else if (asteroide[i].y < 0) {
			asteroide[i].y = asteroide[i].y + 480;
		}
	}
}

void asteroideaMarraztu() {

	for(int i=0;i<10;i++) {
	zirkuluaMarraztu(asteroide[i].x, asteroide[i].y, asteroide[i].radio);

	}

}
int kolisioa(int i, int a, int x, int y) {
	srand(time(NULL));

	float x_berri1 = x + (-10 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri1 = y - (10 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri2 = x + (+7.07106781*cos(a*ROTAZIOA_MURRIZTU) + 7.07106781*sin(a*ROTAZIOA_MURRIZTU));
	float y_berri2 = y - (7.07106781*sin(a*ROTAZIOA_MURRIZTU) - 7.07106781*cos(a*ROTAZIOA_MURRIZTU));
	float x_berri3 = x + (-7.07106781*cos(a*ROTAZIOA_MURRIZTU) + 7.07106781*sin(a*ROTAZIOA_MURRIZTU));
	float y_berri3 = y - (-7.07106781*sin(a*ROTAZIOA_MURRIZTU) - 7.07106781*cos(a*ROTAZIOA_MURRIZTU));
	if (barruanDago(i, x_berri1, y_berri1) != 0 ||
		barruanDago(i, x_berri2, y_berri2) != 0 ||
		barruanDago(i, x_berri3, y_berri3) != 0 ||
		barruanDago(i, (x_berri1 + x_berri2) / 2, (y_berri1 + y_berri2) / 2) ||
		barruanDago(i, (x_berri2 + x_berri3) / 2, (y_berri2 + y_berri3) / 2) ||
		barruanDago(i, (x_berri3 + x_berri1) / 2, (y_berri3 + y_berri1) / 2))
	{
		datuak.bizitzak = datuak.bizitzak - 1;
		for (int h = i; h < datuak.zenbatAsteroidePantailan; h++) {
			asteroide[h].x = asteroide[h + 1].x;
			asteroide[h].y = asteroide[h + 1].y;
			asteroide[h].radio = asteroide[h + 1].radio;
			asteroide[h].vx = asteroide[h + 1].vx;
			asteroide[h].vy = asteroide[h + 1].vy;
		}
		if (asteroide[9].tipo == 3) {
			asteroide[9].x = 1 + rand() % ((640 + 1) - 1);
			asteroide[9].y = 1 + rand() % ((480 + 1) - 1);
			asteroide[9].vx = -2 + rand() % ((2 + 1) + 2);
			asteroide[9].vy = -2 + rand() % ((2 + 1) + 2);
			asteroide[9].radio = 18;
			
			
		}
		
		while (asteroide[9].vx == 0 || asteroide[9].vy == 0) {
			asteroide[9].vx = -2 + rand() % ((2 + 1) + 2);
			asteroide[9].vy = -2 + rand() % ((2 + 1) + 2);
		}

	}
	return datuak.bizitzak;
}
int barruanDago(int i, float x, float y) {
	float dx = x - asteroide[i].x, dy = y - asteroide[i].y;
	return sqrt(dx*dx + (dy*dy)) < asteroide[i].radio;
}
void tiroEgin(int i, int j, float x, float y, float a) {
	tiro[i].x1 = x + (-10 * sin(a*ROTAZIOA_MURRIZTU));
	tiro[i].y1 = y - (10 * cos(a*ROTAZIOA_MURRIZTU));
	tiro[i].x2 = x + (-20 * sin(a*ROTAZIOA_MURRIZTU));
	tiro[i].y2 = y - (20 * cos(a*ROTAZIOA_MURRIZTU));
	tiro[i].vx = 0 + (-10 * sin(a*ROTAZIOA_MURRIZTU));
	tiro[i].vy = 3 - (10 * cos(a*ROTAZIOA_MURRIZTU));


}
void tiroaMarraztu(int i, int j) {
	for (i = 0; i <= j; i++) {
		zuzenaMarraztu(tiro[i].x1, tiro[i].y1, tiro[i].x2, tiro[i].y2);
		tiro[i].x1 = tiro[i].x1 + tiro[i].vx;
		tiro[i].y1 = tiro[i].y1 + tiro[i].vy;
		tiro[i].x2 = tiro[i].x2 + tiro[i].vx;
		tiro[i].y2 = tiro[i].y2 + tiro[i].vy;
	}

}
int kolisioaTiroak(int k, int z) {
	float dx = tiro[k].x2 - asteroide[z].x, dy = tiro[k].y2 - asteroide[z].y;
	return sqrt(dx*dx + (dy*dy)) < asteroide[z].radio;
}
void elementuakEzabatu(int k, int z, int j) {
	for (int i = k; i < j; i++) {
		tiro[i].x1 = tiro[i + 1].x1;
		tiro[i].y1 = tiro[i + 1].y1;
		tiro[i].x2 = tiro[i + 1].x2;
		tiro[i].y2 = tiro[i + 1].y2;
	}
	for (int h = z; h < 9; h++) {
		asteroide[h].x = asteroide[h + 1].x;
		asteroide[h].y = asteroide[h + 1].y;
		asteroide[h].radio = asteroide[h + 1].radio;
	}
	asteroide[9].x = 1 + rand() % ((640 + 1) - 1);
	asteroide[9].y = 1 + rand() % ((480 + 1) - 1);
	asteroide[9].radio = 5 + rand() % ((20 + 1) - 5);
	while (asteroide[9].vx == 0 || asteroide[9].vy == 0) {
		asteroide[9].vx = -2 + rand() % ((2 + 1) + 2);
		asteroide[9].vy = -2 + rand() % ((2 + 1) + 2);
	}
}
void bizitza(int id, int i, int a, int x, int y) {
	if (kolisioa(i, a, x, y) == 3) {
		irudiaMugitu(id, 600, 10);
		irudiakMarraztu();
		irudiaMugitu(id, 570, 10);
		irudiakMarraztu();
		irudiaMugitu(id, 540, 10);
		irudiakMarraztu();
	}
	else if (kolisioa(i, a, x, y) == 2) {
		irudiaMugitu(id, 600, 10);
		irudiakMarraztu();
		irudiaMugitu(id, 570, 10);
		irudiakMarraztu();
	}
	else if (kolisioa(i, a, x, y) == 1) {
		irudiaMugitu(id, 600, 10);
		irudiakMarraztu();
	}
}void ontziaMarraztuSINPLEA(int x, int y, int a) {
	float x_berri1 = x + (-10 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri1 = y - (10 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri2 = x + (+7.07106781*cos(a*ROTAZIOA_MURRIZTU) + 7.07106781*sin(a*ROTAZIOA_MURRIZTU));
	float y_berri2 = y - (7.07106781*sin(a*ROTAZIOA_MURRIZTU) - 7.07106781*cos(a*ROTAZIOA_MURRIZTU));
	float x_berri3 = x + (-7.07106781*cos(a*ROTAZIOA_MURRIZTU) + 7.07106781*sin(a*ROTAZIOA_MURRIZTU));
	float y_berri3 = y - (-7.07106781*sin(a*ROTAZIOA_MURRIZTU) - 7.07106781*cos(a*ROTAZIOA_MURRIZTU));

	arkatzKoloreaEzarri(0, 245, 230);
	zuzenaMarraztu(x_berri1, y_berri1, x_berri2, y_berri2);
	zuzenaMarraztu(x_berri2, y_berri2, x_berri3, y_berri3);
	zuzenaMarraztu(x_berri3, y_berri3, x_berri1, y_berri1);
	
}
void ontziaMarraztuFIGURA(int x, int y, int a) {
	float x_berri1 = x + (-10 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri1 = y - (10 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri2 = x + (3 * cos(a*ROTAZIOA_MURRIZTU) + 2 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri2 = y - (3 * sin(a*ROTAZIOA_MURRIZTU) - 2 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri3 = x + (5 * cos(a*ROTAZIOA_MURRIZTU) + 2 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri3 = y - (5 * sin(a*ROTAZIOA_MURRIZTU) - 2 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri4 = x + (7 * cos(a*ROTAZIOA_MURRIZTU) - 6 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri4 = y - (7 * sin(a*ROTAZIOA_MURRIZTU) + 6 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri5 = x + (10 * cos(a*ROTAZIOA_MURRIZTU) + 7 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri5 = y - (10 * sin(a*ROTAZIOA_MURRIZTU) - 7 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri6 = x + (6 * cos(a*ROTAZIOA_MURRIZTU) + 4 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri6 = y - (6 * sin(a*ROTAZIOA_MURRIZTU) - 4 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri7 = x + (4.5 * cos(a*ROTAZIOA_MURRIZTU) + 4 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri7 = y - (4.5 * sin(a*ROTAZIOA_MURRIZTU) - 4 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri8 = x + (7 * cos(a*ROTAZIOA_MURRIZTU) + 8 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri8 = y - (7 * sin(a*ROTAZIOA_MURRIZTU) - 8 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri9 = x + (-7 * cos(a*ROTAZIOA_MURRIZTU) + 8 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri9 = y - (-7 * sin(a*ROTAZIOA_MURRIZTU) - 8 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri10 = x + (-4.5 * cos(a*ROTAZIOA_MURRIZTU) + 7 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri10 = y - (-4.5 * sin(a*ROTAZIOA_MURRIZTU) - 7 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri11 = x + (-6 * cos(a*ROTAZIOA_MURRIZTU) + 4 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri11 = y - (-6 * sin(a*ROTAZIOA_MURRIZTU) - 4 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri12 = x + (-10 * cos(a*ROTAZIOA_MURRIZTU) + 7 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri12 = y - (-10 * sin(a*ROTAZIOA_MURRIZTU) - 7 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri13 = x + (-7 * cos(a*ROTAZIOA_MURRIZTU) - 6 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri13 = y - (-7 * sin(a*ROTAZIOA_MURRIZTU) + 6 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri14 = x + (-5 * cos(a*ROTAZIOA_MURRIZTU) + 2 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri14 = y - (-5 * sin(a*ROTAZIOA_MURRIZTU) - 2 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri15 = x + (-3 * cos(a*ROTAZIOA_MURRIZTU) + 2 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri15 = y - (-3 * sin(a*ROTAZIOA_MURRIZTU) - 2 * cos(a*ROTAZIOA_MURRIZTU));

	arkatzKoloreaEzarri(0, 245, 230);
	zuzenaMarraztu(x_berri1, y_berri1, x_berri2, y_berri2);
	zuzenaMarraztu(x_berri2, y_berri2, x_berri3, y_berri3);
	zuzenaMarraztu(x_berri3, y_berri3, x_berri4, y_berri4);
	zuzenaMarraztu(x_berri4, y_berri4, x_berri5, y_berri5);
	zuzenaMarraztu(x_berri5, y_berri5, x_berri6, y_berri6);
	zuzenaMarraztu(x_berri6, y_berri6, x_berri7, y_berri7);
	zuzenaMarraztu(x_berri7, y_berri7, x_berri8, y_berri8);
	zuzenaMarraztu(x_berri8, y_berri8, x_berri9, y_berri9);
	zuzenaMarraztu(x_berri9, y_berri9, x_berri10, y_berri10);
	zuzenaMarraztu(x_berri10, y_berri10, x_berri11, y_berri11);
	zuzenaMarraztu(x_berri11, y_berri11, x_berri12, y_berri12);
	zuzenaMarraztu(x_berri12, y_berri12, x_berri13, y_berri13);
	zuzenaMarraztu(x_berri13, y_berri13, x_berri14, y_berri14);
	zuzenaMarraztu(x_berri14, y_berri14, x_berri15, y_berri15);
	zuzenaMarraztu(x_berri15, y_berri15, x_berri1, y_berri1);

}
void ontziaMarraztuSTARWARS(int x, int y, int a)
{
	float x_berri1 = x + (-10 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri1 = y - (10 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri2 = x + (-6 * cos(a*ROTAZIOA_MURRIZTU) + 3 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri2 = y - (-6 * sin(a*ROTAZIOA_MURRIZTU) - 3 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri3 = x + (6 * cos(a*ROTAZIOA_MURRIZTU) + 3 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri3 = y - (6 * sin(a*ROTAZIOA_MURRIZTU) - 3 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri4 = x + (-4 * cos(a*ROTAZIOA_MURRIZTU) + 6 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri4 = y - (-4 * sin(a*ROTAZIOA_MURRIZTU) - 6 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri5 = x + (4 * cos(a*ROTAZIOA_MURRIZTU) + 6 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri5 = y - (4 * sin(a*ROTAZIOA_MURRIZTU) - 6 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri6 = x + (-6 * cos(a*ROTAZIOA_MURRIZTU) - 4 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri6 = y - (-6 * sin(a*ROTAZIOA_MURRIZTU) + 4 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri7 = x + (-6 * cos(a*ROTAZIOA_MURRIZTU) + 8 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri7 = y - (-6 * sin(a*ROTAZIOA_MURRIZTU) - 8 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri8 = x + (6 * cos(a*ROTAZIOA_MURRIZTU) - 4 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri8 = y - (6 * sin(a*ROTAZIOA_MURRIZTU) + 4 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri9 = x + (6 * cos(a*ROTAZIOA_MURRIZTU) + 8 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri9 = y - (6 * sin(a*ROTAZIOA_MURRIZTU) - 8 * cos(a*ROTAZIOA_MURRIZTU));

	arkatzKoloreaEzarri(0, 245, 230);
	zuzenaMarraztu(x_berri1, y_berri1, x_berri2, y_berri2);
	zuzenaMarraztu(x_berri3, y_berri3, x_berri1, y_berri1);
	zuzenaMarraztu(x_berri3, y_berri3, x_berri5, y_berri5);
	zuzenaMarraztu(x_berri1, y_berri1, x_berri2, y_berri2);
	zuzenaMarraztu(x_berri2, y_berri2, x_berri4, y_berri4);
	zuzenaMarraztu(x_berri4, y_berri4, x_berri5, y_berri5);
	zuzenaMarraztu(x_berri6, y_berri6, x_berri7, y_berri7);
	zuzenaMarraztu(x_berri8, y_berri8, x_berri9, y_berri9);
}
void ontziaMarraztuPERTSONAIA(int x, int y, int a)
{
	float x_berri1 = x + (-10 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri1 = y - (10 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri2 = x + (+7.07106781*cos(a*ROTAZIOA_MURRIZTU) + 7.07106781*sin(a*ROTAZIOA_MURRIZTU));
	float y_berri2 = y - (7.07106781*sin(a*ROTAZIOA_MURRIZTU) - 7.07106781*cos(a*ROTAZIOA_MURRIZTU));
	float x_berri3 = x + (-7.07106781*cos(a*ROTAZIOA_MURRIZTU) + 7.07106781*sin(a*ROTAZIOA_MURRIZTU));
	float y_berri3 = y - (-7.07106781*sin(a*ROTAZIOA_MURRIZTU) - 7.07106781*cos(a*ROTAZIOA_MURRIZTU));
	float x_berri4 = x + (-8 * cos(a*ROTAZIOA_MURRIZTU) - 8 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri4 = y - (-8 * sin(a*ROTAZIOA_MURRIZTU) + 8 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri5 = x + (8 * cos(a*ROTAZIOA_MURRIZTU) - 8 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri5 = y - (8 * sin(a*ROTAZIOA_MURRIZTU) + 8 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri6 = x + (-8 * cos(a*ROTAZIOA_MURRIZTU) - 3 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri6 = y - (-8 * sin(a*ROTAZIOA_MURRIZTU) + 3 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri7 = x + (8 * cos(a*ROTAZIOA_MURRIZTU) - 3 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri7 = y - (8 * sin(a*ROTAZIOA_MURRIZTU) + 3 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri8 = x + (-3 * cos(a*ROTAZIOA_MURRIZTU) - 3 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri8 = y - (-3 * sin(a*ROTAZIOA_MURRIZTU) + 3 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri9 = x + (3 * cos(a*ROTAZIOA_MURRIZTU) - 3 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri9 = y - (3 * sin(a*ROTAZIOA_MURRIZTU) + 3 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri10 = x + (-4 * cos(a*ROTAZIOA_MURRIZTU) + 7 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri10 = y - (-4 * sin(a*ROTAZIOA_MURRIZTU) - 7 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri11 = x + (4 * cos(a*ROTAZIOA_MURRIZTU) + 7 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri11 = y - (4 * sin(a*ROTAZIOA_MURRIZTU) - 7 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri12 = x + (-4 * cos(a*ROTAZIOA_MURRIZTU) + 11 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri12 = y - (-4 * sin(a*ROTAZIOA_MURRIZTU) - 11 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri13 = x + (4 * cos(a*ROTAZIOA_MURRIZTU) + 11 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri13 = y - (4 * sin(a*ROTAZIOA_MURRIZTU) - 11 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri14 = x + (+9 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri14 = y - (-9 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri15 = x + (-3 * cos(a*ROTAZIOA_MURRIZTU) + 1 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri15 = y - (-3 * sin(a*ROTAZIOA_MURRIZTU) - 1 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri16 = x + (3 * cos(a*ROTAZIOA_MURRIZTU) + 1 * cos(a*ROTAZIOA_MURRIZTU));
	float y_berri16 = y - (3 * sin(a*ROTAZIOA_MURRIZTU) - 1 * sin(a*ROTAZIOA_MURRIZTU));
	float x_berri17 = x + (-1 * cos(a*ROTAZIOA_MURRIZTU) + 3 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri17 = y - (-1 * sin(a*ROTAZIOA_MURRIZTU) - 3 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri18 = x + (1 * cos(a*ROTAZIOA_MURRIZTU) + 3 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri18 = y - (1 * sin(a*ROTAZIOA_MURRIZTU) - 3 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri19 = x + (-1 * cos(a*ROTAZIOA_MURRIZTU) - 2 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri19 = y - (-1 * sin(a*ROTAZIOA_MURRIZTU) + 2 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri20 = x + (1 * cos(a*ROTAZIOA_MURRIZTU) - 2 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri20 = y - (1 * sin(a*ROTAZIOA_MURRIZTU) + 2 * cos(a*ROTAZIOA_MURRIZTU));

	arkatzKoloreaEzarri(0, 245, 230);
	zuzenaMarraztu(x_berri1, y_berri1, x_berri2, y_berri2);
	zuzenaMarraztu(x_berri3, y_berri3, x_berri1, y_berri1);
	zuzenaMarraztu(x_berri4, y_berri4, x_berri6, y_berri6);
	zuzenaMarraztu(x_berri5, y_berri5, x_berri7, y_berri7);
	zuzenaMarraztu(x_berri7, y_berri7, x_berri9, y_berri9);
	zuzenaMarraztu(x_berri6, y_berri6, x_berri8, y_berri8);
	zuzenaMarraztu(x_berri3, y_berri3, x_berri2, y_berri2);
	zuzenaMarraztu(x_berri10, y_berri10, x_berri12, y_berri12);
	zuzenaMarraztu(x_berri11, y_berri11, x_berri13, y_berri13);
	zuzenaMarraztu(x_berri12, y_berri12, x_berri14, y_berri14);
	zuzenaMarraztu(x_berri12, y_berri13, x_berri14, y_berri14);
	zuzenaMarraztu(x_berri15, y_berri15, x_berri17, y_berri17);
	zuzenaMarraztu(x_berri18, y_berri18, x_berri17, y_berri17);
	zuzenaMarraztu(x_berri16, y_berri16, x_berri18, y_berri18);
	zirkuluaMarraztu(x_berri19, y_berri19, 0.5);
	zirkuluaMarraztu(x_berri20, y_berri20, 0.5);
}
void ontziaMarraztuKOROA(int x, int y, int a)
{

	float x_berri1 = x + (-10 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri1 = y - (10 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri2 = x + (-4 * cos(a*ROTAZIOA_MURRIZTU) + 4 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri2 = y - (-4 * sin(a*ROTAZIOA_MURRIZTU) - 4 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri3 = x + (4 * cos(a*ROTAZIOA_MURRIZTU) + 4 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri3 = y - (4 * sin(a*ROTAZIOA_MURRIZTU) - 4 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri4 = x + (-7.07106781 * cos(a*ROTAZIOA_MURRIZTU) + 1 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri4 = y - (-7.07106781 * sin(a*ROTAZIOA_MURRIZTU) - 1 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri5 = x + (7.07106781 * cos(a*ROTAZIOA_MURRIZTU) + 1 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri5 = y - (7.07106781 * sin(a*ROTAZIOA_MURRIZTU) - 1 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri6 = x + (-7.07106781 * cos(a*ROTAZIOA_MURRIZTU) + 7.07106781 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri6 = y - (-7.07106781 * sin(a*ROTAZIOA_MURRIZTU) - 7.07106781 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri7 = x + (7.07106781 * cos(a*ROTAZIOA_MURRIZTU) + 7.07106781 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri7 = y - (7.07106781 * sin(a*ROTAZIOA_MURRIZTU) - 7.07106781 * cos(a*ROTAZIOA_MURRIZTU));



	arkatzKoloreaEzarri(0, 245, 230);
	zuzenaMarraztu(x_berri1, y_berri1, x_berri2, y_berri2);
	zuzenaMarraztu(x_berri3, y_berri3, x_berri1, y_berri1);
	zuzenaMarraztu(x_berri3, y_berri3, x_berri5, y_berri5);
	zuzenaMarraztu(x_berri2, y_berri2, x_berri4, y_berri4);
	zuzenaMarraztu(x_berri4, y_berri4, x_berri6, y_berri6);
	zuzenaMarraztu(x_berri5, y_berri5, x_berri7, y_berri7);
	zuzenaMarraztu(x_berri6, y_berri6, x_berri7, y_berri7);
}
void ontziaMarraztuMU(int x, int y, int a)
{

	float x_berri1 = x + (-5 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri1 = y - (+5 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri2 = x + (-6 * cos(a*ROTAZIOA_MURRIZTU) - 8 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri2 = y - (-6 * sin(a*ROTAZIOA_MURRIZTU) + 8 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri3 = x + (6 * cos(a*ROTAZIOA_MURRIZTU) - 8 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri3 = y - (6 * sin(a*ROTAZIOA_MURRIZTU) + 8 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri4 = x + (-6 * cos(a*ROTAZIOA_MURRIZTU) + 8 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri4 = y - (-6 * sin(a*ROTAZIOA_MURRIZTU) - 8 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri5 = x + (6 * cos(a*ROTAZIOA_MURRIZTU) + 8 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri5 = y - (6 * sin(a*ROTAZIOA_MURRIZTU) - 8 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri6 = x + (-2 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri6 = y - (+2 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri7 = x + (-3 * cos(a*ROTAZIOA_MURRIZTU) - 4 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri7 = y - (-3 * sin(a*ROTAZIOA_MURRIZTU) + 4 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri8 = x + (3 * cos(a*ROTAZIOA_MURRIZTU) - 4 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri8 = y - (3 * sin(a*ROTAZIOA_MURRIZTU) + 4 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri9 = x + (-3 * cos(a*ROTAZIOA_MURRIZTU) + 5 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri9 = y - (-3 * sin(a*ROTAZIOA_MURRIZTU) - 5 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri10 = x + (3 * cos(a*ROTAZIOA_MURRIZTU) + 5 * sin(a*ROTAZIOA_MURRIZTU));
	float y_berri10 = y - (3 * sin(a*ROTAZIOA_MURRIZTU) - 5 * cos(a*ROTAZIOA_MURRIZTU));
	float x_berri11 = x + (3 * cos(a*ROTAZIOA_MURRIZTU));
	float y_berri11 = y - (3 * sin(a*ROTAZIOA_MURRIZTU));
	float x_berri12 = x + (-3 * cos(a*ROTAZIOA_MURRIZTU));
	float y_berri12 = y - (-3 * sin(a*ROTAZIOA_MURRIZTU));
	float x_berri13 = x + (6 * cos(a*ROTAZIOA_MURRIZTU));
	float y_berri13 = y - (6 * sin(a*ROTAZIOA_MURRIZTU));
	float x_berri14 = x + (-6 * cos(a*ROTAZIOA_MURRIZTU));
	float y_berri14 = y - (-6 * sin(a*ROTAZIOA_MURRIZTU));

	arkatzKoloreaEzarri(0, 245, 230);
	zuzenaMarraztu(x_berri1, y_berri1, x_berri2, y_berri2);
	zuzenaMarraztu(x_berri3, y_berri3, x_berri1, y_berri1);
	zuzenaMarraztu(x_berri2, y_berri2, x_berri4, y_berri4);
	zuzenaMarraztu(x_berri3, y_berri3, x_berri5, y_berri5);
	zuzenaMarraztu(x_berri4, y_berri4, x_berri5, y_berri5);
	zuzenaMarraztu(x_berri6, y_berri6, x_berri7, y_berri7);
	zuzenaMarraztu(x_berri8, y_berri8, x_berri6, y_berri6);
	zuzenaMarraztu(x_berri7, y_berri7, x_berri9, y_berri9);
	zuzenaMarraztu(x_berri8, y_berri8, x_berri10, y_berri10);
	zuzenaMarraztu(x_berri9, y_berri9, x_berri10, y_berri10);
	zuzenaMarraztu(x_berri12, y_berri12, x_berri14, y_berri14);
	zuzenaMarraztu(x_berri11, y_berri11, x_berri13, y_berri13);
}

void zailtasuna() {
	int i = 0;
	if (datuak.score < 450)datuak.zailtasuna = 1;
	if (datuak.score >= 450 && datuak.score < 1500)datuak.zailtasuna = 2;
	if (datuak.score >= 1500 + 650 * i && datuak.score < 2150 + 650 * i) {
		datuak.zailtasuna++;
		i++;
		datuak.zenbatAsteroidePantailan++;
		asteroideBerriaSortu();
	}

}
void hasierakoAsteroideakKalkulatu() {
	srand(time(NULL));
	int i = 0;
	while (i < 2) {
		asteroide[i].x = 1 + rand() % ((640 + 1) - 1);
		asteroide[i].y = 1 + rand() % ((480 + 1) - 1);
		asteroide[i].tipo = 3;
		asteroide[i].radio = 30;		

		while (asteroide[i].vx == 0 || asteroide[i].vy == 0) {
			asteroide[i].vx = -2 + rand() % ((2 + 1) + 2);
			asteroide[i].vy = -2 + rand() % ((2 + 1) + 2);
		}
		i++;
	}
}
void asteroideBerriaSortu() {
	srand(time(NULL));
		asteroide[datuak.zenbatAsteroidePantailan].x = 1 + rand() % ((640 + 1) - 1);
		asteroide[datuak.zenbatAsteroidePantailan].y = 1 + rand() % ((480 + 1) - 1);
		asteroide[datuak.zenbatAsteroidePantailan].tipo = 3;
		asteroide[datuak.zenbatAsteroidePantailan].radio = 30;

		while (asteroide[datuak.zenbatAsteroidePantailan].vx == 0 || asteroide[datuak.zenbatAsteroidePantailan].vy == 0) {
			asteroide[datuak.zenbatAsteroidePantailan].vx = -2 + rand() % ((2 + 1) + 2);
			asteroide[datuak.zenbatAsteroidePantailan].vy = -2 + rand() % ((2 + 1) + 2);
		}
}