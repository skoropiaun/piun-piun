#include "SDL.h"
struct ONTZIA {
	float a, y, x, vy, vx, ax, ay;
	int dx;
	int dy;
}ontzia;
struct ASTEROIDE {
	int x;
	int y;
	int radio;
	int vx;
	int vy;
	int tipo;
}asteroide[20];
struct TIRO {
	int x1, x2;
	int y1, y2;
	int vx, vy;
}tiro[10];

void ontziaHasieratu();
void datuakHasieratu();
void ontziaMarraztuSINPLEA(int x, int y, int a);
void ontziaMarraztuFIGURA(int x, int y, int a);
void ontziaMarraztuSTARWARS(int x, int y, int a);
void ontziaMarraztuPERTSONAIA(int x, int y, int a);
void ontziaMarraztuKOROA(int x, int y, int a);
void ontziaMarraztuMU(int x, int y, int a);

//void asteroideaKalkulatu();
void asteroideaMarraztu();
void asteroideaMugitu();
int kolisioa(int i, int a, int x, int y);
int barruanDago(int i, float x, float y);
void tiroEgin(int i, int j, float x, float y, float a);
void tiroaMarraztu(int i, int j);
int kolisioaTiroak(int k, int z);
void elementuakEzabatu(int k, int z, int j);
void bizitza(int id, int i, int a, int x, int y);
void zailtasuna();
void hasierakoAsteroideakKalkulatu();
void asteroideBerriaSortu();


int irudiaMarraztu(SDL_Texture* texture, SDL_Rect *pDest);



