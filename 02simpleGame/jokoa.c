#include "jokoa.h"
#include "imagen.h"
#include "graphics.h"
#include "ebentoak.h"
#include "text.h"
#include "soinua.h"
#include <stdio.h>
#include <windows.h>
#include <stdlib.h> 
#include <time.h>




#include "abilitateak.h"
#include "asteroideak.h"
#include "elementuakHasieratu.h"
#include "kolisioak.h"
#include "ontzia.h"
#include "tiroak.h"
#include "konstanteak.h"
#include "OurTypes.h"

#define _CRT_SECURE_NO_WARNINGS
#define ONGI_ETORRI_MEZUA "Sakatu return hasteko..."
#define JOKOA_SOUND ".\\sound\\musikaJulen.mp3"
#define JOKOA_BIZITZAK ".\\img\\bizitza.bmp"
#define PIUN ".\\sound\\piun.wav"
#define ABIADURA ".\\img\\abiadura.bmp"
#define TIROAK ".\\img\\tiroak.bmp"
#define BABESA ".\\img\\babesa.bmp"



EGOERA jokatu(void)
{
	srand((unsigned int)time(NULL));
	char puntuazioa[10];
	int mugitu = 0; //boolean
	EGOERA  egoera = JOLASTEN;
	ontziaHasieratu();
	datuakHasieratu();
	asteroideakHasieratu();
	int ebentu = 0;

	

	audioInit();
	loadTheMusic(JOKOA_SOUND);
	playMusic();
	int idBizitzak = irudiaKargatu(JOKOA_BIZITZAK);
	int idPiun = loadSound(PIUN);
	int idAbiadura = irudiaKargatu(ABIADURA);
	int idTiroak = irudiaKargatu(TIROAK);
	int idBabesa = irudiaKargatu(BABESA);



	while (egoera == JOLASTEN)
	{
		//MUGIMENDUA SORTZEKO

		ontzia.x = ontzia.x + ontzia.vx;
		ontzia.y = ontzia.y + ontzia.vy;
		//ONTZIA PANTAILAREN ALDE BATETIK BESTERA JOATEKO

		if (ontzia.x > 640) {
			ontzia.x = ontzia.x - 640;
		}
		else if (ontzia.x < 0) {
			ontzia.x = ontzia.x + 640;
		}
		if (ontzia.y > 480) {
			ontzia.y = ontzia.y - 480;
		}
		else if (ontzia.y < 0) {
			ontzia.y = ontzia.y + 480;
		}

		// TEKLAREN ARABERA ONTZIA MUGITU, ROTATZEKO, TIROA EGITEKO ETA ABILITATEAK
		Sleep(10);
		switch (ebentu) {
			case TECLA_RIGHT: ontzia.a = ontzia.a - 1;
				break;
			case TECLA_LEFT: ontzia.a = ontzia.a + 1;
				break;
			case TECLA_UP: ontzia.ax = (-0.25 * sin(ontzia.a*ROTAZIOA_MURRIZTU));
				ontzia.ay = -(0.25 * cos(ontzia.a*ROTAZIOA_MURRIZTU));
				ontzia.vx = ontzia.vx + ontzia.ax;
				ontzia.vy = ontzia.vy + ontzia.ay;
				break;
			case TECLA_DOWN: ontzia.ax = (-0.25 * sin(ontzia.a*ROTAZIOA_MURRIZTU));
				ontzia.ay = -(0.25 * cos(ontzia.a*ROTAZIOA_MURRIZTU));
				ontzia.vx = ontzia.vx - ontzia.ax;
				ontzia.vy = ontzia.vy - ontzia.ay;
				break;
			case TECLA_SPACE:playSound(idPiun);
				if (datuak.zenbatTiro > datuak.tiroMaximoak) { datuak.tiroa = 0; datuak.zenbatTiro = 0; }
				tiroEgin(ontzia.x, ontzia.y, ontzia.a);
				datuak.tiroa++;
				datuak.zenbatTiro++;
				break;
			case TECLA_q:
				if (datuak.zenbatTiro +12 > datuak.tiroMaximoak) { datuak.tiroa = 0; datuak.zenbatTiro = 0; }
				if (datuak.tiroAbilitatea != 0) 
				{
				playSound(idPiun);
				abilitateTiroa((int)ontzia.x, (int)ontzia.y);
				datuak.tiroAbilitatea--;
				}
				break;
			case TECLA_w: 
				if (datuak.abiaduraMoteldu != 0) 
				{
				abilitateAsteroideenAbiaduraMoteldu();
				datuak.abiaduraMoteldu--;
				}
				break;
		}
		
		
		//BIZITAK  0 IZATEN BADIRA AZKEN PANTAILARA ERAMATEKO
		for (int i = 0; i < datuak.zenbatPantailan; i++) {
			if (kolisioa(i, ontzia.a, ontzia.x, ontzia.y) == 0) {
				egoera = GALDU;
			}
		}
		//TIROAK ASTEROIDEA JOTZEN BADU

		for (int zenbatTiro = 0; zenbatTiro <= datuak.zenbatTiro; zenbatTiro++) {
			for (int z = 0; z < datuak.zenbatPantailan; z++) {
				if (kolisioaTiroak(zenbatTiro, z) != 0) {
					elementuakEzabatu(zenbatTiro, z);
				}
			}
		}

		sprintf(puntuazioa, "%d", datuak.score);

		pantailaGarbitu();
		bizitza(idBizitzak);
		abilitateak(idAbiadura,idBabesa,idTiroak);
		textuaIdatzi(10, 10, puntuazioa);
		arkatzKoloreaEzarri(255, 255, 255);
		tiroaMarraztu();
		arkatzKoloreaEzarri(125, 140, 140);
		asteroideaMarraztu();
		asteroideaMugitu();
		arkatzKoloreaEzarri(0, 255, 255);
		if (datuak.babesa != 0) {
			arkatzKoloreaEzarri(45, 231, 17);
		}

		// ONTZIA JOKOAN ZEIN IZANGO DEN KARGATZEKO
		switch (ontzia.zenbakia) {

		case 0: ontziaMarraztuSTARWARS((float)ontzia.x, (float)ontzia.y, (float)ontzia.a);
			break;
		case 1: ontziaMarraztuKOROA((float)ontzia.x, (float)ontzia.y, (float)ontzia.a);
			break;
		case 2: ontziaMarraztuFIGURA((float)ontzia.x, (float)ontzia.y, (float)ontzia.a);
			break;
		case 3: ontziaMarraztuMU((float)ontzia.x, (float)ontzia.y,(float)ontzia.a);
			break;
		case 4: ontziaMarraztuPERTSONAIA((float)ontzia.x, (float)ontzia.y, (float)ontzia.a);
			break;
		case 5: ontziaMarraztuSINPLEA((float)ontzia.x, (float)ontzia.y, (float)ontzia.a);
			break;
		}
		
		puntuenAraberaAsteroideaSortu();
		pantailaBerriztu();
		ebentu = ebentuaJasoGertatuBada();
		// ARRATOIA IKUTUZ GERO AZKEN PANTAILARA ERAMATEKO

		if (ebentu == SAGU_BOTOIA_ESKUMA || ebentu == SAGU_BOTOIA_EZKERRA)
		{
			egoera = IRABAZI;
		}
	}
	toggleMusic();
	audioTerminate();
	pantailaGarbitu();
	pantailaBerriztu();
	return egoera;
}