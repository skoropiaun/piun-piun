#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "soinua.h"

#include "SDL_ttf.h"

#include"konstanteak.h"
#include "OurTypes.h"
#include "ontzia.h"
#include "kolisioak.h"
#include "tiroak.h"
#include "asteroideak.h"
#include "elementuakHasieratu.h"
#include "imagen.h"


//ONTZIAK ASTEROIDEA IKUTZEN BADU
int barruanDago(int i, double x, double y) {
	double dx = x - asteroide[i].x, dy = y - asteroide[i].y;
	return sqrt(dx*dx + (dy*dy)) < asteroide[i].radio;
}
//TIROA ASTEROIDEA IKUTZEN BADU
int kolisioaTiroak(int k, int z) {
	double dx = tiro[k].x2 - asteroide[z].x, dy = tiro[k].y2 - asteroide[z].y;
	return sqrt(dx*dx + (dy*dy)) <= asteroide[z].radio;
}
//TIROAK ASTEROIDEA EMATEAN ASTEROIDE HORI EZABATZEKO.
void elementuakEzabatu(int k, int z) {
	
	int tipotmp = asteroide[z].tipo;
	double tmpx = asteroide[z].x;
	double tmpy = asteroide[z].y;
	puntuatu(tipotmp);
	
	for (int i = k; i < datuak.zenbatTiro; i++) {
		tiro[i].x1 = tiro[i + 1].x1;
		tiro[i].y1 = tiro[i + 1].y1;
		tiro[i].x2 = tiro[i + 1].x2;
		tiro[i].y2 = tiro[i + 1].y2;
		tiro[i].vx = tiro[i + 1].vx;
		tiro[i].vy = tiro[i + 1].vy;
	}
	datuak.zenbatTiro--;
	for (int h = z; h < datuak.zenbatPantailan; h++) {

		asteroide[h].x = asteroide[h + 1].x;
		asteroide[h].y = asteroide[h + 1].y;
		asteroide[h].radio = asteroide[h + 1].radio;
		asteroide[h].vx = asteroide[h + 1].vx;
		asteroide[h].vy = asteroide[h + 1].vy;
		asteroide[h].tipo = asteroide[h + 1].tipo;
		for (int j = 0; j < 12; j++) {
			asteroide[h].puntua[j].y = asteroide[h + 1].puntua[j].y;
			asteroide[h].puntua[j].x = asteroide[h + 1].puntua[j].x;
		}
	}
	asteroideBerriaKalkulatu(tipotmp,(int) tmpx,(int) tmpy);
	datuak.zenbatPantailan++;
	asteroideBerriaKalkulatu(tipotmp,(int) tmpx,(int) tmpy);
}
// BZIZITZAK ZENBAT PANTAILARATU BEHAR DIREN
void bizitza(int id) {
	switch (datuak.bizitzak) {
		case 1:irudiaMugitu(id, 600, 10);
			irudiakMarraztu();
			break;
		case 2:irudiaMugitu(id, 600, 10);
			irudiakMarraztu();
			irudiaMugitu(id, 570, 10);
			irudiakMarraztu();
			break;
		case 3: irudiaMugitu(id, 600, 10);
			irudiakMarraztu();
			irudiaMugitu(id, 570, 10);
			irudiakMarraztu();
			irudiaMugitu(id, 540, 10);
			irudiakMarraztu();
			break;
	}
}
// APURTUTAKO ASTEROIDEAREN ARABERA ZENBAT PUNTU IRABAZIKO DITUZUN
void puntuatu(int tipo) {
	switch (tipo) {
	case 0: datuak.score = datuak.score + 90;
		break;
	case 1: datuak.score = datuak.score + 54;
		break;
	case 2: datuak.score = datuak.score + 30;
		break;
	case 3: datuak.score = datuak.score + 18;
		break;
	}

}
// ONTZIAREN ARABERA ZEIN KOLISIO KARGATU BEHAR DEN JAKITEKO

int kolisioa(int i, double a, double x, double y) {
	int erantzuna = 0;


	switch (ontzia.zenbakia) {

	case 0: erantzuna = kolisioaSTARWARS(i, a, x, y);
		break;
	case 1: erantzuna = kolisioaKOROA(i, a, x, y);
		break;
	case 2: erantzuna = kolisioaFIGURA(i, a, x, y);
		break;
	case 3: erantzuna = kolisioaMU(i, a, x, y);
		break;
	case 4: erantzuna = kolisioaPERTSONAIA(i, a, x, y);
		break;
	case 5: erantzuna = kolisioaSINPLE(i, a, x, y);
		break;
	}
	return erantzuna;
}

//ONTZI BAKOITZAREN KOLISIOAK

int kolisioaSINPLE(int i, double a, double x, double y) {



	int tipotmp = asteroide[i].tipo;
	int tmpx = 1 + rand() % ((640 + 1) - 1);
	int tmpy = 1 + rand() % ((480 + 1) - 1);

	double x_berri1 = x + (-10 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri1 = y - (10 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri2 = x + (+7.07106781*cos(a*ROTAZIOA_MURRIZTU) + 7.07106781*sin(a*ROTAZIOA_MURRIZTU));
	double y_berri2 = y - (7.07106781*sin(a*ROTAZIOA_MURRIZTU) - 7.07106781*cos(a*ROTAZIOA_MURRIZTU));
	double x_berri3 = x + (-7.07106781*cos(a*ROTAZIOA_MURRIZTU) + 7.07106781*sin(a*ROTAZIOA_MURRIZTU));
	double y_berri3 = y - (-7.07106781*sin(a*ROTAZIOA_MURRIZTU) - 7.07106781*cos(a*ROTAZIOA_MURRIZTU));
	if (barruanDago(i, x_berri1, y_berri1) != 0 ||
		barruanDago(i, x_berri2, y_berri2) != 0 ||
		barruanDago(i, x_berri3, y_berri3) != 0 ||
		barruanDago(i, (x_berri1 + x_berri2) / 2, (y_berri1 + y_berri2) / 2) ||
		barruanDago(i, (x_berri2 + x_berri3) / 2, (y_berri2 + y_berri3) / 2) ||
		barruanDago(i, (x_berri3 + x_berri1) / 2, (y_berri3 + y_berri1) / 2))
	{
		if (datuak.babesa != 0) {
			datuak.babesa--;
		}
		else { datuak.bizitzak--; }
		
		for (int h = i; h < datuak.zenbatPantailan; h++) {

			asteroide[h].x = asteroide[h + 1].x;
			asteroide[h].y = asteroide[h + 1].y;
			asteroide[h].radio = asteroide[h + 1].radio;
			asteroide[h].vx = asteroide[h + 1].vx;
			asteroide[h].vy = asteroide[h + 1].vy;
			asteroide[h].tipo = asteroide[h + 1].tipo;
			for (int j = 0; j < 12; j++) {
				asteroide[h].puntua[j].y = asteroide[h + 1].puntua[j].y;
				asteroide[h].puntua[j].x = asteroide[h + 1].puntua[j].x;
			}
		}
		asteroideBerriaKalkulatu(tipotmp, tmpx, tmpy);
		datuak.zenbatPantailan++;
		asteroideBerriaKalkulatu(tipotmp, tmpx, tmpy);

	}
	return datuak.bizitzak;
}
int kolisioaFIGURA(int i, double a, double x, double y) {

	int tipotmp = asteroide[i].tipo;
	int tmpx = 1 + rand() % ((640 + 1) - 1);
	int tmpy = 1 + rand() % ((480 + 1) - 1);

	double x_berri1 = x + (-10 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri1 = y - (10 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri2 = x + (3 * cos(a*ROTAZIOA_MURRIZTU) + 2 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri2 = y - (3 * sin(a*ROTAZIOA_MURRIZTU) - 2 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri3 = x + (5 * cos(a*ROTAZIOA_MURRIZTU) + 2 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri3 = y - (5 * sin(a*ROTAZIOA_MURRIZTU) - 2 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri4 = x + (7 * cos(a*ROTAZIOA_MURRIZTU) - 6 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri4 = y - (7 * sin(a*ROTAZIOA_MURRIZTU) + 6 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri5 = x + (10 * cos(a*ROTAZIOA_MURRIZTU) + 7 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri5 = y - (10 * sin(a*ROTAZIOA_MURRIZTU) - 7 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri6 = x + (6 * cos(a*ROTAZIOA_MURRIZTU) + 4 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri6 = y - (6 * sin(a*ROTAZIOA_MURRIZTU) - 4 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri7 = x + (4.5 * cos(a*ROTAZIOA_MURRIZTU) + 4 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri7 = y - (4.5 * sin(a*ROTAZIOA_MURRIZTU) - 4 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri8 = x + (7 * cos(a*ROTAZIOA_MURRIZTU) + 8 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri8 = y - (7 * sin(a*ROTAZIOA_MURRIZTU) - 8 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri9 = x + (-7 * cos(a*ROTAZIOA_MURRIZTU) + 8 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri9 = y - (-7 * sin(a*ROTAZIOA_MURRIZTU) - 8 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri10 = x + (-4.5 * cos(a*ROTAZIOA_MURRIZTU) + 7 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri10 = y - (-4.5 * sin(a*ROTAZIOA_MURRIZTU) - 7 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri11 = x + (-6 * cos(a*ROTAZIOA_MURRIZTU) + 4 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri11 = y - (-6 * sin(a*ROTAZIOA_MURRIZTU) - 4 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri12 = x + (-10 * cos(a*ROTAZIOA_MURRIZTU) + 7 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri12 = y - (-10 * sin(a*ROTAZIOA_MURRIZTU) - 7 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri13 = x + (-7 * cos(a*ROTAZIOA_MURRIZTU) - 6 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri13 = y - (-7 * sin(a*ROTAZIOA_MURRIZTU) + 6 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri14 = x + (-5 * cos(a*ROTAZIOA_MURRIZTU) + 2 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri14 = y - (-5 * sin(a*ROTAZIOA_MURRIZTU) - 2 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri15 = x + (-3 * cos(a*ROTAZIOA_MURRIZTU) + 2 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri15 = y - (-3 * sin(a*ROTAZIOA_MURRIZTU) - 2 * cos(a*ROTAZIOA_MURRIZTU));
	if (barruanDago(i, x_berri1, y_berri1) != 0 ||
		barruanDago(i, x_berri2, y_berri2) != 0 ||
		barruanDago(i, x_berri3, y_berri3) != 0 ||
		barruanDago(i, x_berri4, y_berri4) != 0 ||
		barruanDago(i, x_berri5, y_berri5) != 0 ||
		barruanDago(i, x_berri6, y_berri6) != 0 ||
		barruanDago(i, x_berri7, y_berri7) != 0 ||
		barruanDago(i, x_berri8, y_berri8) != 0 ||
		barruanDago(i, x_berri9, y_berri9) != 0 ||
		barruanDago(i, x_berri10, y_berri10) != 0 ||
		barruanDago(i, x_berri11, y_berri11) != 0 ||
		barruanDago(i, x_berri12, y_berri12) != 0 ||
		barruanDago(i, x_berri13, y_berri13) != 0 ||
		barruanDago(i, x_berri14, y_berri14) != 0 ||
		barruanDago(i, x_berri15, y_berri15) != 0)
	{
		if (datuak.babesa != 0) {
			datuak.babesa--;
		}
		else { datuak.bizitzak--; }
		for (int h = i; h < datuak.zenbatPantailan; h++) {

			asteroide[h].x = asteroide[h + 1].x;
			asteroide[h].y = asteroide[h + 1].y;
			asteroide[h].radio = asteroide[h + 1].radio;
			asteroide[h].vx = asteroide[h + 1].vx;
			asteroide[h].vy = asteroide[h + 1].vy;
			asteroide[h].tipo = asteroide[h + 1].tipo;
			for (int j = 0; j < 12; j++) {
				asteroide[h].puntua[j].y = asteroide[h + 1].puntua[j].y;
				asteroide[h].puntua[j].x = asteroide[h + 1].puntua[j].x;
			}
		}
		asteroideBerriaKalkulatu(tipotmp, tmpx, tmpy);
		datuak.zenbatPantailan++;
		asteroideBerriaKalkulatu(tipotmp, tmpx, tmpy);

	}
	return datuak.bizitzak;
}
int kolisioaSTARWARS(int i, double a, double x, double y) {

	int tipotmp = asteroide[i].tipo;
	int tmpx = 1 + rand() % ((640 + 1) - 1);
	int tmpy = 1 + rand() % ((480 + 1) - 1);

	double x_berri1 = x + (-10 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri1 = y - (10 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri2 = x + (-6 * cos(a*ROTAZIOA_MURRIZTU) + 3 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri2 = y - (-6 * sin(a*ROTAZIOA_MURRIZTU) - 3 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri3 = x + (6 * cos(a*ROTAZIOA_MURRIZTU) + 3 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri3 = y - (6 * sin(a*ROTAZIOA_MURRIZTU) - 3 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri4 = x + (-4 * cos(a*ROTAZIOA_MURRIZTU) + 6 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri4 = y - (-4 * sin(a*ROTAZIOA_MURRIZTU) - 6 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri5 = x + (4 * cos(a*ROTAZIOA_MURRIZTU) + 6 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri5 = y - (4 * sin(a*ROTAZIOA_MURRIZTU) - 6 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri6 = x + (-6 * cos(a*ROTAZIOA_MURRIZTU) - 4 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri6 = y - (-6 * sin(a*ROTAZIOA_MURRIZTU) + 4 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri7 = x + (-6 * cos(a*ROTAZIOA_MURRIZTU) + 8 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri7 = y - (-6 * sin(a*ROTAZIOA_MURRIZTU) - 8 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri8 = x + (6 * cos(a*ROTAZIOA_MURRIZTU) - 4 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri8 = y - (6 * sin(a*ROTAZIOA_MURRIZTU) + 4 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri9 = x + (6 * cos(a*ROTAZIOA_MURRIZTU) + 8 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri9 = y - (6 * sin(a*ROTAZIOA_MURRIZTU) - 8 * cos(a*ROTAZIOA_MURRIZTU));
	if (barruanDago(i, x_berri1, y_berri1) != 0 ||
		barruanDago(i, x_berri2, y_berri2) != 0 ||
		barruanDago(i, x_berri3, y_berri3) != 0 ||
		barruanDago(i, x_berri4, y_berri4) != 0 ||
		barruanDago(i, x_berri5, y_berri5) != 0 ||
		barruanDago(i, x_berri6, y_berri6) != 0 ||
		barruanDago(i, x_berri7, y_berri7) != 0 ||
		barruanDago(i, x_berri8, y_berri8) != 0 ||
		barruanDago(i, x_berri9, y_berri9) != 0)
	{
		if (datuak.babesa != 0) {
			datuak.babesa--;
		}
		else { datuak.bizitzak--; }
		for (int h = i; h < datuak.zenbatPantailan; h++) {

			asteroide[h].x = asteroide[h + 1].x;
			asteroide[h].y = asteroide[h + 1].y;
			asteroide[h].radio = asteroide[h + 1].radio;
			asteroide[h].vx = asteroide[h + 1].vx;
			asteroide[h].vy = asteroide[h + 1].vy;
			asteroide[h].tipo = asteroide[h + 1].tipo;
			for (int j = 0; j < 12; j++) {
				asteroide[h].puntua[j].y = asteroide[h + 1].puntua[j].y;
				asteroide[h].puntua[j].x = asteroide[h + 1].puntua[j].x;
			}
		}
		asteroideBerriaKalkulatu(tipotmp, tmpx, tmpy);
		datuak.zenbatPantailan++;
		asteroideBerriaKalkulatu(tipotmp, tmpx, tmpy);

	}
	return datuak.bizitzak;
}
int kolisioaPERTSONAIA(int i, double a, double x, double y) {

	int tipotmp = asteroide[i].tipo;
	int tmpx = 1 + rand() % ((640 + 1) - 1);
	int tmpy = 1 + rand() % ((480 + 1) - 1);

	double x_berri1 = x + (-10 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri1 = y - (10 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri2 = x + (+7.07106781*cos(a*ROTAZIOA_MURRIZTU) + 7.07106781*sin(a*ROTAZIOA_MURRIZTU));
	double y_berri2 = y - (7.07106781*sin(a*ROTAZIOA_MURRIZTU) - 7.07106781*cos(a*ROTAZIOA_MURRIZTU));
	double x_berri3 = x + (-7.07106781*cos(a*ROTAZIOA_MURRIZTU) + 7.07106781*sin(a*ROTAZIOA_MURRIZTU));
	double y_berri3 = y - (-7.07106781*sin(a*ROTAZIOA_MURRIZTU) - 7.07106781*cos(a*ROTAZIOA_MURRIZTU));
	double x_berri4 = x + (-8 * cos(a*ROTAZIOA_MURRIZTU) - 8 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri4 = y - (-8 * sin(a*ROTAZIOA_MURRIZTU) + 8 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri5 = x + (8 * cos(a*ROTAZIOA_MURRIZTU) - 8 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri5 = y - (8 * sin(a*ROTAZIOA_MURRIZTU) + 8 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri6 = x + (-8 * cos(a*ROTAZIOA_MURRIZTU) - 3 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri6 = y - (-8 * sin(a*ROTAZIOA_MURRIZTU) + 3 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri7 = x + (8 * cos(a*ROTAZIOA_MURRIZTU) - 3 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri7 = y - (8 * sin(a*ROTAZIOA_MURRIZTU) + 3 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri8 = x + (-3 * cos(a*ROTAZIOA_MURRIZTU) - 3 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri8 = y - (-3 * sin(a*ROTAZIOA_MURRIZTU) + 3 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri9 = x + (3 * cos(a*ROTAZIOA_MURRIZTU) - 3 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri9 = y - (3 * sin(a*ROTAZIOA_MURRIZTU) + 3 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri10 = x + (-4 * cos(a*ROTAZIOA_MURRIZTU) + 7 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri10 = y - (-4 * sin(a*ROTAZIOA_MURRIZTU) - 7 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri11 = x + (4 * cos(a*ROTAZIOA_MURRIZTU) + 7 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri11 = y - (4 * sin(a*ROTAZIOA_MURRIZTU) - 7 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri12 = x + (-4 * cos(a*ROTAZIOA_MURRIZTU) + 11 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri12 = y - (-4 * sin(a*ROTAZIOA_MURRIZTU) - 11 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri13 = x + (4 * cos(a*ROTAZIOA_MURRIZTU) + 11 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri13 = y - (4 * sin(a*ROTAZIOA_MURRIZTU) - 11 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri14 = x + (+9 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri14 = y - (-9 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri15 = x + (-3 * cos(a*ROTAZIOA_MURRIZTU) + 1 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri15 = y - (-3 * sin(a*ROTAZIOA_MURRIZTU) - 1 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri16 = x + (3 * cos(a*ROTAZIOA_MURRIZTU) + 1 * cos(a*ROTAZIOA_MURRIZTU));
	double y_berri16 = y - (3 * sin(a*ROTAZIOA_MURRIZTU) - 1 * sin(a*ROTAZIOA_MURRIZTU));
	double x_berri17 = x + (-1 * cos(a*ROTAZIOA_MURRIZTU) + 3 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri17 = y - (-1 * sin(a*ROTAZIOA_MURRIZTU) - 3 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri18 = x + (1 * cos(a*ROTAZIOA_MURRIZTU) + 3 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri18 = y - (1 * sin(a*ROTAZIOA_MURRIZTU) - 3 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri19 = x + (-1 * cos(a*ROTAZIOA_MURRIZTU) - 2 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri19 = y - (-1 * sin(a*ROTAZIOA_MURRIZTU) + 2 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri20 = x + (1 * cos(a*ROTAZIOA_MURRIZTU) - 2 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri20 = y - (1 * sin(a*ROTAZIOA_MURRIZTU) + 2 * cos(a*ROTAZIOA_MURRIZTU));
	if (barruanDago(i, x_berri1, y_berri1) != 0 ||
		barruanDago(i, x_berri2, y_berri2) != 0 ||
		barruanDago(i, x_berri3, y_berri3) != 0 ||
		barruanDago(i, x_berri4, y_berri4) != 0 ||
		barruanDago(i, x_berri5, y_berri5) != 0 ||
		barruanDago(i, x_berri6, y_berri6) != 0 ||
		barruanDago(i, x_berri7, y_berri7) != 0 ||
		barruanDago(i, x_berri8, y_berri8) != 0 ||
		barruanDago(i, x_berri9, y_berri9) != 0 ||
		barruanDago(i, x_berri10, y_berri10) != 0 ||
		barruanDago(i, x_berri11, y_berri11) != 0 ||
		barruanDago(i, x_berri12, y_berri12) != 0 ||
		barruanDago(i, x_berri13, y_berri13) != 0 ||
		barruanDago(i, x_berri14, y_berri14) != 0 ||
		barruanDago(i, x_berri15, y_berri15) != 0 ||
		barruanDago(i, x_berri16, y_berri16) != 0 ||
		barruanDago(i, x_berri17, y_berri17) != 0 ||
		barruanDago(i, x_berri18, y_berri18) != 0 ||
		barruanDago(i, x_berri19, y_berri19) != 0 ||
		barruanDago(i, x_berri20, y_berri20) != 0)
	{
		if (datuak.babesa != 0) {
			datuak.babesa--;
		}
		else { datuak.bizitzak--; }
		for (int h = i; h < datuak.zenbatPantailan; h++) {

			asteroide[h].x = asteroide[h + 1].x;
			asteroide[h].y = asteroide[h + 1].y;
			asteroide[h].radio = asteroide[h + 1].radio;
			asteroide[h].vx = asteroide[h + 1].vx;
			asteroide[h].vy = asteroide[h + 1].vy;
			asteroide[h].tipo = asteroide[h + 1].tipo;
			for (int j = 0; j < 12; j++) {
				asteroide[h].puntua[j].y = asteroide[h + 1].puntua[j].y;
				asteroide[h].puntua[j].x = asteroide[h + 1].puntua[j].x;
			}
		}
		asteroideBerriaKalkulatu(tipotmp, tmpx, tmpy);
		datuak.zenbatPantailan++;
		asteroideBerriaKalkulatu(tipotmp, tmpx, tmpy);

	}
	return datuak.bizitzak;
}
int kolisioaKOROA(int i, double a, double x, double y) {

	int tipotmp = asteroide[i].tipo;
	int tmpx = 1 + rand() % ((640 + 1) - 1);
	int tmpy = 1 + rand() % ((480 + 1) - 1);

	double x_berri1 = x + (-10 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri1 = y - (10 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri2 = x + (-4 * cos(a*ROTAZIOA_MURRIZTU) + 4 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri2 = y - (-4 * sin(a*ROTAZIOA_MURRIZTU) - 4 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri3 = x + (4 * cos(a*ROTAZIOA_MURRIZTU) + 4 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri3 = y - (4 * sin(a*ROTAZIOA_MURRIZTU) - 4 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri4 = x + (-7.07106781 * cos(a*ROTAZIOA_MURRIZTU) + 1 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri4 = y - (-7.07106781 * sin(a*ROTAZIOA_MURRIZTU) - 1 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri5 = x + (7.07106781 * cos(a*ROTAZIOA_MURRIZTU) + 1 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri5 = y - (7.07106781 * sin(a*ROTAZIOA_MURRIZTU) - 1 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri6 = x + (-7.07106781 * cos(a*ROTAZIOA_MURRIZTU) + 7.07106781 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri6 = y - (-7.07106781 * sin(a*ROTAZIOA_MURRIZTU) - 7.07106781 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri7 = x + (7.07106781 * cos(a*ROTAZIOA_MURRIZTU) + 7.07106781 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri7 = y - (7.07106781 * sin(a*ROTAZIOA_MURRIZTU) - 7.07106781 * cos(a*ROTAZIOA_MURRIZTU));
	if (barruanDago(i, x_berri1, y_berri1) != 0 ||
		barruanDago(i, x_berri2, y_berri2) != 0 ||
		barruanDago(i, x_berri3, y_berri3) != 0 ||
		barruanDago(i, x_berri4, y_berri4) != 0 ||
		barruanDago(i, x_berri5, y_berri5) != 0 ||
		barruanDago(i, x_berri6, y_berri6) != 0 ||
		barruanDago(i, x_berri7, y_berri7) != 0)
	{
		if (datuak.babesa != 0) {
			datuak.babesa--;
		}
		else { datuak.bizitzak--; }
		for (int h = i; h < datuak.zenbatPantailan; h++) {

			asteroide[h].x = asteroide[h + 1].x;
			asteroide[h].y = asteroide[h + 1].y;
			asteroide[h].radio = asteroide[h + 1].radio;
			asteroide[h].vx = asteroide[h + 1].vx;
			asteroide[h].vy = asteroide[h + 1].vy;
			asteroide[h].tipo = asteroide[h + 1].tipo;
			for (int j = 0; j < 12; j++) {
				asteroide[h].puntua[j].y = asteroide[h + 1].puntua[j].y;
				asteroide[h].puntua[j].x = asteroide[h + 1].puntua[j].x;
			}
		}
		asteroideBerriaKalkulatu(tipotmp, tmpx, tmpy);
		datuak.zenbatPantailan++;
		asteroideBerriaKalkulatu(tipotmp, tmpx, tmpy);

	}
	return datuak.bizitzak;
}
int kolisioaMU(int i, double a, double x, double y) {

	int tipotmp = asteroide[i].tipo;
	int tmpx = 1 + rand() % ((640 + 1) - 1);
	int tmpy = 1 + rand() % ((480 + 1) - 1);

	double x_berri1 = x + (-5 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri1 = y - (+5 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri2 = x + (-6 * cos(a*ROTAZIOA_MURRIZTU) - 8 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri2 = y - (-6 * sin(a*ROTAZIOA_MURRIZTU) + 8 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri3 = x + (6 * cos(a*ROTAZIOA_MURRIZTU) - 8 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri3 = y - (6 * sin(a*ROTAZIOA_MURRIZTU) + 8 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri4 = x + (-6 * cos(a*ROTAZIOA_MURRIZTU) + 8 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri4 = y - (-6 * sin(a*ROTAZIOA_MURRIZTU) - 8 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri5 = x + (6 * cos(a*ROTAZIOA_MURRIZTU) + 8 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri5 = y - (6 * sin(a*ROTAZIOA_MURRIZTU) - 8 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri6 = x + (-2 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri6 = y - (+2 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri7 = x + (-3 * cos(a*ROTAZIOA_MURRIZTU) - 4 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri7 = y - (-3 * sin(a*ROTAZIOA_MURRIZTU) + 4 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri8 = x + (3 * cos(a*ROTAZIOA_MURRIZTU) - 4 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri8 = y - (3 * sin(a*ROTAZIOA_MURRIZTU) + 4 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri9 = x + (-3 * cos(a*ROTAZIOA_MURRIZTU) + 5 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri9 = y - (-3 * sin(a*ROTAZIOA_MURRIZTU) - 5 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri10 = x + (3 * cos(a*ROTAZIOA_MURRIZTU) + 5 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri10 = y - (3 * sin(a*ROTAZIOA_MURRIZTU) - 5 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri11 = x + (3 * cos(a*ROTAZIOA_MURRIZTU));
	double y_berri11 = y - (3 * sin(a*ROTAZIOA_MURRIZTU));
	double x_berri12 = x + (-3 * cos(a*ROTAZIOA_MURRIZTU));
	double y_berri12 = y - (-3 * sin(a*ROTAZIOA_MURRIZTU));
	double x_berri13 = x + (6 * cos(a*ROTAZIOA_MURRIZTU));
	double y_berri13 = y - (6 * sin(a*ROTAZIOA_MURRIZTU));
	double x_berri14 = x + (-6 * cos(a*ROTAZIOA_MURRIZTU));
	double y_berri14 = y - (-6 * sin(a*ROTAZIOA_MURRIZTU));
	double x_berri15 = x + (-6 * cos(a*ROTAZIOA_MURRIZTU) + 8 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri15 = y - (-6 * sin(a*ROTAZIOA_MURRIZTU) - 8 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri16 = x + (6 * cos(a*ROTAZIOA_MURRIZTU) + 8 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri16 = y - (6 * sin(a*ROTAZIOA_MURRIZTU) - 8 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri17 = x + (-3 * cos(a*ROTAZIOA_MURRIZTU) + 5 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri17 = y - (-3 * sin(a*ROTAZIOA_MURRIZTU) - 5 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri18 = x + (3 * cos(a*ROTAZIOA_MURRIZTU) + 5 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri18 = y - (3 * sin(a*ROTAZIOA_MURRIZTU) - 5 * cos(a*ROTAZIOA_MURRIZTU));
	if (barruanDago(i, x_berri1, y_berri1) != 0 ||
		barruanDago(i, x_berri2, y_berri2) != 0 ||
		barruanDago(i, x_berri3, y_berri3) != 0 ||
		barruanDago(i, x_berri4, y_berri4) != 0 ||
		barruanDago(i, x_berri5, y_berri5) != 0 ||
		barruanDago(i, x_berri6, y_berri6) != 0 ||
		barruanDago(i, x_berri7, y_berri7) != 0 ||
		barruanDago(i, x_berri8, y_berri8) != 0 ||
		barruanDago(i, x_berri9, y_berri9) != 0 ||
		barruanDago(i, x_berri10, y_berri10) != 0 ||
		barruanDago(i, x_berri11, y_berri11) != 0 ||
		barruanDago(i, x_berri12, y_berri12) != 0 ||
		barruanDago(i, x_berri13, y_berri13) != 0 ||
		barruanDago(i, x_berri14, y_berri14) != 0 || 
		barruanDago(i, x_berri15, y_berri15) != 0 || 
		barruanDago(i, x_berri16, y_berri16) != 0 || 
		barruanDago(i, x_berri17, y_berri17) != 0 || 
		barruanDago(i, x_berri18, y_berri18) != 0)
	{
		if (datuak.babesa != 0) {
			datuak.babesa--;
		}
		else { datuak.bizitzak--; }
		for (int h = i; h < datuak.zenbatPantailan; h++) {

			asteroide[h].x = asteroide[h + 1].x;
			asteroide[h].y = asteroide[h + 1].y;
			asteroide[h].radio = asteroide[h + 1].radio;
			asteroide[h].vx = asteroide[h + 1].vx;
			asteroide[h].vy = asteroide[h + 1].vy;
			asteroide[h].tipo = asteroide[h + 1].tipo;
			for (int j = 0; j < 12; j++) {
				asteroide[h].puntua[j].y = asteroide[h + 1].puntua[j].y;
				asteroide[h].puntua[j].x = asteroide[h + 1].puntua[j].x;
			}
		}
		asteroideBerriaKalkulatu(tipotmp, tmpx, tmpy);
		datuak.zenbatPantailan++;
		asteroideBerriaKalkulatu(tipotmp, tmpx, tmpy);

	}
	return datuak.bizitzak;
}

