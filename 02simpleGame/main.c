/*#include "orokorrak.h"
#include "sarrera.h"
#include "errealitateFisikoa.h"
#include "jokoa.h"
#include "bukaera.h"*/

//---------------------------------------------------------------------------------
#include "imagen.h"
#include "graphics.h"
#include "ebentoak.h"
#include "text.h"
#include "soinua.h"
//---------------------------------------------------------------------------------

#include <stdio.h>

#include "ourTypes.h"
#include "jokoa.h"

int jarraitu = 0;
int score = 0;

int main(int argc, char * str[]) {


	EGOERA egoera;

	if (sgHasieratu() == -1)
	{
		fprintf(stderr, "Unable to set 640x480 video: %s\n", SDL_GetError());
		return 1;
	}
	textuaGaitu();
	do
	{
		jokoaAurkeztu();
		egoera = jokatu();
		jarraitu = jokoAmaierakoa(egoera);
	} while (egoera);
	sgItxi();
	return 0;
}


