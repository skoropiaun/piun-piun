#include "jokoa.h"
#include "imagen.h"
#include "graphics.h"
#include "ebentoak.h"
#include "text.h"
#include "soinua.h"
#include <stdio.h>
#include <windows.h>
#include "konstanteak.h"
#include "menu.h"
#include "elementuakHasieratu.h"
#include "ontzia.h"




#define JOKOA_BIZITZAK ".\\img\\bizitza.bmp"
#define PIUM_PIUM_MEZUA "PIUM-PIUM"
#define JOLASTU_MEZUA "PLAY"
#define EXIT_MEZUA "EXIT"
#define FONDO_IMAGE ".\\img\\FONDOA.bmp"
#define FLECHA ".\\img\\flecha .bmp"
#define NAVE1 ".\\img\\nave1.bmp"
#define NAVE2 ".\\img\\nave2.bmp"
#define NAVE3 ".\\img\\nave3.bmp"
#define NAVE4 ".\\img\\nave4.bmp"
#define NAVE5 ".\\img\\nave5.bmp"
#define NAVE0 ".\\img\\simplea.bmp"
#define MENU_SOINUA ".\\sound\\misc_menu_3.wav"
#define MENU_SOINUA2 ".\\sound\\misc_menu.wav"
#define INSTRUKZIOAK ".\\img\\ArauakDef.bmp"


int arratoiarenPosizioaMenuan = 0, atera = 2;

void jokoaAurkeztu(void)
{
	int ebentu = 0, idFondokoIrudia, idFlechaExit, idFlechaJolastu, idOntzienArgazkiak, idinstrukzioak;
	audioInit();
	int idMenuSoinua = loadSound(MENU_SOINUA);
	int idMenuSoinua2 = loadSound(MENU_SOINUA2);



	idFondokoIrudia = Fondoko_Irudia();
	idFlechaExit = MENU_FLECHA_Exit();
	irudiaKendu(idFlechaExit);
	idFlechaJolastu = MENU_FLECHA_Jolastu();

	ontzia.zenbakia = 5;
	do
	{
		ebentu = ebentuaJasoGertatuBada();
		sarreraMezuaIdatzi();
		pantailaBerriztu();
		idOntzienArgazkiak = ontziarenArgazkiak(ontzia.zenbakia);

		if (ebentu == TECLA_DOWN && arratoiarenPosizioaMenuan == 0)
		{
			playSound(idMenuSoinua);
			arratoiarenPosizioaMenuan = 1;
			irudiaKendu(idFlechaJolastu);
			idFlechaExit = MENU_FLECHA_Exit();
		}
		if (ebentu == TECLA_UP && arratoiarenPosizioaMenuan == 1)
		{
			playSound(idMenuSoinua);
			arratoiarenPosizioaMenuan = 0;
			irudiaKendu(idFlechaExit);
			idFlechaJolastu = MENU_FLECHA_Jolastu();
		}
		if (ebentu == TECLA_LEFT)
		{
			playSound(idMenuSoinua2);
			if (ontzia.zenbakia != 0)
			{
				ontzia.zenbakia--;
			}
			else
			{
				ontzia.zenbakia  = 5;
			}
				
		}
		if (ebentu == TECLA_RIGHT)
		{
			playSound(idMenuSoinua2);

			if (ontzia.zenbakia != 5)
			{
				ontzia.zenbakia++;
			}
			else
			{
				ontzia.zenbakia = 0;
			}

		}
		
		irudiaKendu(idOntzienArgazkiak);
		
		
		
	} while (ebentu != TECLA_RETURN);
	
	if (arratoiarenPosizioaMenuan == 1)
	{
		sgItxi();
	}

	pantailaGarbitu();
	pantailaBerriztu();
	irudiaKendu(idFondokoIrudia);
	irudiaKendu(idFlechaJolastu);
	
	

	idinstrukzioak = arauak_irudia();
	
	do
	{
		ebentu = ebentuaJasoGertatuBada();

	} while (ebentu != TECLA_RETURN);

	irudiaKendu(idinstrukzioak);
	


}
//EXIT ETA JOLASTU MEZUAK IDAZTEKO FUNTZIO HAU. TESTUA AUTATU FUNTZIOAK LETRA HORIA
void sarreraMezuaIdatzi()
{
	if (arratoiarenPosizioaMenuan == 0)
	{
		textuaAutatu(300, 330, JOLASTU_MEZUA);
		textuaIdatzi(300, 400, EXIT_MEZUA);

	}
	if (arratoiarenPosizioaMenuan == 1)
	{
		textuaIdatzi(300, 330, JOLASTU_MEZUA);
		textuaAutatu(300, 400, EXIT_MEZUA);
	}
	pantailaBerriztu();
}

//MENUKO IRUDI GUZTIAK KARGATZEKO ETA TOKIA AUKERATZEKO
int Fondoko_Irudia() {
	int id2 = -1;
	id2 = irudiaKargatu(FONDO_IMAGE);
	irudiaMugitu(id2, 0, 0);

	irudiakMarraztu();
	pantailaBerriztu();
	return id2;
}
int MENU_FLECHA_Jolastu() {
	int id3 = -1;
	pantailaGarbitu();
	id3 = irudiaKargatu(FLECHA);
	irudiaMugitu(id3, 200, 310);
	irudiakMarraztu();
	pantailaBerriztu();
	return id3;
}
int MENU_FLECHA_Exit() {
	int id3 = -1;
	pantailaGarbitu();
	id3 = irudiaKargatu(FLECHA);
	irudiaMugitu(id3, 200, 380);
	irudiakMarraztu();
	pantailaBerriztu();
	return id3;
}

int arauak_irudia() {
	int id3 = -1;
	pantailaGarbitu();
	id3 = irudiaKargatu(INSTRUKZIOAK);
	irudiaMugitu(id3, 0, 0);
	irudiakMarraztu();
	pantailaBerriztu();
	return id3;
}
//ESPAZIONTZIEN IRUDIA AUKERATU ETA KARGATZEKO.
int ontziarenArgazkiak(int ontziarenposizioa)
{
	int id3 = -1;
	switch (ontziarenposizioa) {
	case 0: id3 = irudiaKargatu(NAVE4);
		break;
	case 1: id3 = irudiaKargatu(NAVE1);
		break;
	case 2: id3 = irudiaKargatu(NAVE2);
		break;
	case 3: id3 = irudiaKargatu(NAVE3);
		break;
	case 4: id3 = irudiaKargatu(NAVE5);
		break;
	case 5: id3 = irudiaKargatu(NAVE0);
		break;
	}
	
	
	irudiaMugitu(id3, 270, 170);
	irudiakMarraztu();
	
	return id3;
}

