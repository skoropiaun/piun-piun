#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "SDL_ttf.h"

#include "graphics.h"
#include"konstanteak.h"
#include "ontzia.h"
#include "OurTypes.h"

// ONTZIAREN X ETA Y BERE POSIZIOA. A ONTZIAREN ERROTAZIOA KALKULATZEKO

void ontziaMarraztuSINPLEA(float x, float y, float a) {
	double x_berri1 = x + (-10 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri1 = y - (10 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri2 = x + (+7.07106781*cos(a*ROTAZIOA_MURRIZTU) + 7.07106781*sin(a*ROTAZIOA_MURRIZTU));
	double y_berri2 = y - (7.07106781*sin(a*ROTAZIOA_MURRIZTU) - 7.07106781*cos(a*ROTAZIOA_MURRIZTU));
	double x_berri3 = x + (-7.07106781*cos(a*ROTAZIOA_MURRIZTU) + 7.07106781*sin(a*ROTAZIOA_MURRIZTU));
	double y_berri3 = y - (-7.07106781*sin(a*ROTAZIOA_MURRIZTU) - 7.07106781*cos(a*ROTAZIOA_MURRIZTU));


	zuzenaMarraztu((int)x_berri1, (int)y_berri1, (int)x_berri2, (int)y_berri2);
	zuzenaMarraztu((int)x_berri2, (int)y_berri2, (int)x_berri3, (int)y_berri3);
	zuzenaMarraztu((int)x_berri3, (int)y_berri3, (int)x_berri1, (int)y_berri1);

}
void ontziaMarraztuFIGURA(float x, float y, float a) {
	double x_berri1 = x + (-10 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri1 = y - (10 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri2 = x + (3 * cos(a*ROTAZIOA_MURRIZTU) + 2 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri2 = y - (3 * sin(a*ROTAZIOA_MURRIZTU) - 2 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri3 = x + (5 * cos(a*ROTAZIOA_MURRIZTU) + 2 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri3 = y - (5 * sin(a*ROTAZIOA_MURRIZTU) - 2 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri4 = x + (7 * cos(a*ROTAZIOA_MURRIZTU) - 6 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri4 = y - (7 * sin(a*ROTAZIOA_MURRIZTU) + 6 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri5 = x + (10 * cos(a*ROTAZIOA_MURRIZTU) + 7 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri5 = y - (10 * sin(a*ROTAZIOA_MURRIZTU) - 7 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri6 = x + (6 * cos(a*ROTAZIOA_MURRIZTU) + 4 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri6 = y - (6 * sin(a*ROTAZIOA_MURRIZTU) - 4 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri7 = x + (4.5 * cos(a*ROTAZIOA_MURRIZTU) + 4 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri7 = y - (4.5 * sin(a*ROTAZIOA_MURRIZTU) - 4 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri8 = x + (7 * cos(a*ROTAZIOA_MURRIZTU) + 8 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri8 = y - (7 * sin(a*ROTAZIOA_MURRIZTU) - 8 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri9 = x + (-7 * cos(a*ROTAZIOA_MURRIZTU) + 8 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri9 = y - (-7 * sin(a*ROTAZIOA_MURRIZTU) - 8 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri10 = x + (-4.5 * cos(a*ROTAZIOA_MURRIZTU) + 7 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri10 = y - (-4.5 * sin(a*ROTAZIOA_MURRIZTU) - 7 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri11 = x + (-6 * cos(a*ROTAZIOA_MURRIZTU) + 4 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri11 = y - (-6 * sin(a*ROTAZIOA_MURRIZTU) - 4 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri12 = x + (-10 * cos(a*ROTAZIOA_MURRIZTU) + 7 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri12 = y - (-10 * sin(a*ROTAZIOA_MURRIZTU) - 7 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri13 = x + (-7 * cos(a*ROTAZIOA_MURRIZTU) - 6 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri13 = y - (-7 * sin(a*ROTAZIOA_MURRIZTU) + 6 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri14 = x + (-5 * cos(a*ROTAZIOA_MURRIZTU) + 2 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri14 = y - (-5 * sin(a*ROTAZIOA_MURRIZTU) - 2 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri15 = x + (-3 * cos(a*ROTAZIOA_MURRIZTU) + 2 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri15 = y - (-3 * sin(a*ROTAZIOA_MURRIZTU) - 2 * cos(a*ROTAZIOA_MURRIZTU));

	
	zuzenaMarraztu((int)x_berri1, (int)y_berri1, (int)x_berri2, (int)y_berri2);
	zuzenaMarraztu((int)x_berri2, (int)y_berri2, (int)x_berri3, (int)y_berri3);
	zuzenaMarraztu((int)x_berri3, (int)y_berri3, (int)x_berri4, (int)y_berri4);
	zuzenaMarraztu((int)x_berri4, (int)y_berri4, (int)x_berri5, (int)y_berri5);
	zuzenaMarraztu((int)x_berri5, (int)y_berri5, (int)x_berri6, (int)y_berri6);
	zuzenaMarraztu((int)x_berri6, (int)y_berri6, (int)x_berri7, (int)y_berri7);
	zuzenaMarraztu((int)x_berri7, (int)y_berri7, (int)x_berri8, (int)y_berri8);
	zuzenaMarraztu((int)x_berri8, (int)y_berri8, (int)x_berri9, (int)y_berri9);
	zuzenaMarraztu((int)x_berri9, (int)y_berri9, (int)x_berri10, (int)y_berri10);
	zuzenaMarraztu((int)x_berri10, (int)y_berri10, (int)x_berri11, (int)y_berri11);
	zuzenaMarraztu((int)x_berri11, (int)y_berri11, (int)x_berri12, (int)y_berri12);
	zuzenaMarraztu((int)x_berri12, (int)y_berri12, (int)x_berri13, (int)y_berri13);
	zuzenaMarraztu((int)x_berri13, (int)y_berri13, (int)x_berri14, (int)y_berri14);
	zuzenaMarraztu((int)x_berri14, (int)y_berri14, (int)x_berri15, (int)y_berri15);
	zuzenaMarraztu((int)x_berri15, (int)y_berri15, (int)x_berri1, (int)y_berri1);

}
void ontziaMarraztuSTARWARS(float x, float y, float a)
{
	double x_berri1 = x + (-10 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri1 = y - (10 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri2 = x + (-6 * cos(a*ROTAZIOA_MURRIZTU) + 3 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri2 = y - (-6 * sin(a*ROTAZIOA_MURRIZTU) - 3 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri3 = x + (6 * cos(a*ROTAZIOA_MURRIZTU) + 3 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri3 = y - (6 * sin(a*ROTAZIOA_MURRIZTU) - 3 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri4 = x + (-4 * cos(a*ROTAZIOA_MURRIZTU) + 6 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri4 = y - (-4 * sin(a*ROTAZIOA_MURRIZTU) - 6 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri5 = x + (4 * cos(a*ROTAZIOA_MURRIZTU) + 6 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri5 = y - (4 * sin(a*ROTAZIOA_MURRIZTU) - 6 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri6 = x + (-6 * cos(a*ROTAZIOA_MURRIZTU) - 4 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri6 = y - (-6 * sin(a*ROTAZIOA_MURRIZTU) + 4 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri7 = x + (-6 * cos(a*ROTAZIOA_MURRIZTU) + 8 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri7 = y - (-6 * sin(a*ROTAZIOA_MURRIZTU) - 8 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri8 = x + (6 * cos(a*ROTAZIOA_MURRIZTU) - 4 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri8 = y - (6 * sin(a*ROTAZIOA_MURRIZTU) + 4 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri9 = x + (6 * cos(a*ROTAZIOA_MURRIZTU) + 8 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri9 = y - (6 * sin(a*ROTAZIOA_MURRIZTU) - 8 * cos(a*ROTAZIOA_MURRIZTU));

	
	zuzenaMarraztu((int)x_berri1, (int)y_berri1, (int)x_berri2, (int)y_berri2);
	zuzenaMarraztu((int)x_berri3, (int)y_berri3, (int)x_berri1, (int)y_berri1);
	zuzenaMarraztu((int)x_berri3, (int)y_berri3, (int)x_berri5, (int)y_berri5);
	zuzenaMarraztu((int)x_berri1, (int)y_berri1, (int)x_berri2, (int)y_berri2);
	zuzenaMarraztu((int)x_berri2, (int)y_berri2, (int)x_berri4, (int)y_berri4);
	zuzenaMarraztu((int)x_berri4, (int)y_berri4, (int)x_berri5, (int)y_berri5);
	zuzenaMarraztu((int)x_berri6, (int)y_berri6, (int)x_berri7, (int)y_berri7);
	zuzenaMarraztu((int)x_berri8, (int)y_berri8, (int)x_berri9, (int)y_berri9);
}
void ontziaMarraztuPERTSONAIA(float x, float y, float a)
{
	double x_berri1 = x + (-10 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri1 = y - (10 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri2 = x + (+7.07106781*cos(a*ROTAZIOA_MURRIZTU) + 7.07106781*sin(a*ROTAZIOA_MURRIZTU));
	double y_berri2 = y - (7.07106781*sin(a*ROTAZIOA_MURRIZTU) - 7.07106781*cos(a*ROTAZIOA_MURRIZTU));
	double x_berri3 = x + (-7.07106781*cos(a*ROTAZIOA_MURRIZTU) + 7.07106781*sin(a*ROTAZIOA_MURRIZTU));
	double y_berri3 = y - (-7.07106781*sin(a*ROTAZIOA_MURRIZTU) - 7.07106781*cos(a*ROTAZIOA_MURRIZTU));
	double x_berri4 = x + (-8 * cos(a*ROTAZIOA_MURRIZTU) - 8 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri4 = y - (-8 * sin(a*ROTAZIOA_MURRIZTU) + 8 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri5 = x + (8 * cos(a*ROTAZIOA_MURRIZTU) - 8 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri5 = y - (8 * sin(a*ROTAZIOA_MURRIZTU) + 8 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri6 = x + (-8 * cos(a*ROTAZIOA_MURRIZTU) - 3 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri6 = y - (-8 * sin(a*ROTAZIOA_MURRIZTU) + 3 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri7 = x + (8 * cos(a*ROTAZIOA_MURRIZTU) - 3 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri7 = y - (8 * sin(a*ROTAZIOA_MURRIZTU) + 3 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri8 = x + (-3 * cos(a*ROTAZIOA_MURRIZTU) - 3 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri8 = y - (-3 * sin(a*ROTAZIOA_MURRIZTU) + 3 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri9 = x + (3 * cos(a*ROTAZIOA_MURRIZTU) - 3 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri9 = y - (3 * sin(a*ROTAZIOA_MURRIZTU) + 3 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri10 = x + (-4 * cos(a*ROTAZIOA_MURRIZTU) + 7 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri10 = y - (-4 * sin(a*ROTAZIOA_MURRIZTU) - 7 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri11 = x + (4 * cos(a*ROTAZIOA_MURRIZTU) + 7 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri11 = y - (4 * sin(a*ROTAZIOA_MURRIZTU) - 7 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri12 = x + (-4 * cos(a*ROTAZIOA_MURRIZTU) + 11 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri12 = y - (-4 * sin(a*ROTAZIOA_MURRIZTU) - 11 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri13 = x + (4 * cos(a*ROTAZIOA_MURRIZTU) + 11 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri13 = y - (4 * sin(a*ROTAZIOA_MURRIZTU) - 11 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri14 = x + (+9 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri14 = y - (-9 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri15 = x + (-3 * cos(a*ROTAZIOA_MURRIZTU) + 1 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri15 = y - (-3 * sin(a*ROTAZIOA_MURRIZTU) - 1 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri16 = x + (3 * cos(a*ROTAZIOA_MURRIZTU) + 1 * cos(a*ROTAZIOA_MURRIZTU));
	double y_berri16 = y - (3 * sin(a*ROTAZIOA_MURRIZTU) - 1 * sin(a*ROTAZIOA_MURRIZTU));
	double x_berri17 = x + (-1 * cos(a*ROTAZIOA_MURRIZTU) + 3 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri17 = y - (-1 * sin(a*ROTAZIOA_MURRIZTU) - 3 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri18 = x + (1 * cos(a*ROTAZIOA_MURRIZTU) + 3 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri18 = y - (1 * sin(a*ROTAZIOA_MURRIZTU) - 3 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri19 = x + (-1 * cos(a*ROTAZIOA_MURRIZTU) - 2 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri19 = y - (-1 * sin(a*ROTAZIOA_MURRIZTU) + 2 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri20 = x + (1 * cos(a*ROTAZIOA_MURRIZTU) - 2 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri20 = y - (1 * sin(a*ROTAZIOA_MURRIZTU) + 2 * cos(a*ROTAZIOA_MURRIZTU));

	
	zuzenaMarraztu((int)x_berri1, (int)y_berri1, (int)x_berri2, (int)y_berri2);
	zuzenaMarraztu((int)x_berri3, (int)y_berri3, (int)x_berri1, (int)y_berri1);
	zuzenaMarraztu((int)x_berri4, (int)y_berri4, (int)x_berri6, (int)y_berri6);
	zuzenaMarraztu((int)x_berri5, (int)y_berri5, (int)x_berri7, (int)y_berri7);
	zuzenaMarraztu((int)x_berri7, (int)y_berri7, (int)x_berri9, (int)y_berri9);
	zuzenaMarraztu((int)x_berri6, (int)y_berri6, (int)x_berri8, (int)y_berri8);
	zuzenaMarraztu((int)x_berri3, (int)y_berri3, (int)x_berri2, (int)y_berri2);
	zuzenaMarraztu((int)x_berri10, (int)y_berri10, (int)x_berri12, (int)y_berri12);
	zuzenaMarraztu((int)x_berri11, (int)y_berri11, (int)x_berri13, (int)y_berri13);
	zuzenaMarraztu((int)x_berri12, (int)y_berri12, (int)x_berri14, (int)y_berri14);
	zuzenaMarraztu((int)x_berri13, (int)y_berri13, (int)x_berri14, (int)y_berri14);
	zuzenaMarraztu((int)x_berri15, (int)y_berri15, (int)x_berri17, (int)y_berri17);
	zuzenaMarraztu((int)x_berri18, (int)y_berri18, (int)x_berri17, (int)y_berri17);
	zuzenaMarraztu((int)x_berri16, (int)y_berri16, (int)x_berri18, (int)y_berri18);
	zirkuluaMarraztu((int)x_berri19, (int)y_berri19, 1);
	zirkuluaMarraztu((int)x_berri20, (int)y_berri20, 1);
}
void ontziaMarraztuKOROA(float x, float y, float a)
{

	double x_berri1 = x + (-10 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri1 = y - (10 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri2 = x + (-4 * cos(a*ROTAZIOA_MURRIZTU) + 4 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri2 = y - (-4 * sin(a*ROTAZIOA_MURRIZTU) - 4 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri3 = x + (4 * cos(a*ROTAZIOA_MURRIZTU) + 4 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri3 = y - (4 * sin(a*ROTAZIOA_MURRIZTU) - 4 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri4 = x + (-7.07106781 * cos(a*ROTAZIOA_MURRIZTU) + 1 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri4 = y - (-7.07106781 * sin(a*ROTAZIOA_MURRIZTU) - 1 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri5 = x + (7.07106781 * cos(a*ROTAZIOA_MURRIZTU) + 1 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri5 = y - (7.07106781 * sin(a*ROTAZIOA_MURRIZTU) - 1 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri6 = x + (-7.07106781 * cos(a*ROTAZIOA_MURRIZTU) + 7.07106781 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri6 = y - (-7.07106781 * sin(a*ROTAZIOA_MURRIZTU) - 7.07106781 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri7 = x + (7.07106781 * cos(a*ROTAZIOA_MURRIZTU) + 7.07106781 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri7 = y - (7.07106781 * sin(a*ROTAZIOA_MURRIZTU) - 7.07106781 * cos(a*ROTAZIOA_MURRIZTU));



	
	zuzenaMarraztu((int)x_berri1, (int)y_berri1, (int)x_berri2, (int)y_berri2);
	zuzenaMarraztu((int)x_berri3, (int)y_berri3, (int)x_berri1, (int)y_berri1);
	zuzenaMarraztu((int)x_berri3, (int)y_berri3, (int)x_berri5, (int)y_berri5);
	zuzenaMarraztu((int)x_berri2, (int)y_berri2, (int)x_berri4, (int)y_berri4);
	zuzenaMarraztu((int)x_berri4, (int)y_berri4, (int)x_berri6, (int)y_berri6);
	zuzenaMarraztu((int)x_berri5, (int)y_berri5, (int)x_berri7, (int)y_berri7);
	zuzenaMarraztu((int)x_berri6, (int)y_berri6, (int)x_berri7,(int) y_berri7);
}
void ontziaMarraztuMU(float x, float y, float a)
{

	double x_berri1 = x + (-5 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri1 = y - (+5 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri2 = x + (-6 * cos(a*ROTAZIOA_MURRIZTU) - 8 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri2 = y - (-6 * sin(a*ROTAZIOA_MURRIZTU) + 8 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri3 = x + (6 * cos(a*ROTAZIOA_MURRIZTU) - 8 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri3 = y - (6 * sin(a*ROTAZIOA_MURRIZTU) + 8 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri4 = x + (-6 * cos(a*ROTAZIOA_MURRIZTU) + 8 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri4 = y - (-6 * sin(a*ROTAZIOA_MURRIZTU) - 8 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri5 = x + (6 * cos(a*ROTAZIOA_MURRIZTU) + 8 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri5 = y - (6 * sin(a*ROTAZIOA_MURRIZTU) - 8 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri6 = x + (-2 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri6 = y - (+2 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri7 = x + (-3 * cos(a*ROTAZIOA_MURRIZTU) - 4 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri7 = y - (-3 * sin(a*ROTAZIOA_MURRIZTU) + 4 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri8 = x + (3 * cos(a*ROTAZIOA_MURRIZTU) - 4 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri8 = y - (3 * sin(a*ROTAZIOA_MURRIZTU) + 4 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri9 = x + (-3 * cos(a*ROTAZIOA_MURRIZTU) + 5 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri9 = y - (-3 * sin(a*ROTAZIOA_MURRIZTU) - 5 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri10 = x + (3 * cos(a*ROTAZIOA_MURRIZTU) + 5 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri10 = y - (3 * sin(a*ROTAZIOA_MURRIZTU) - 5 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri11 = x + (3 * cos(a*ROTAZIOA_MURRIZTU));
	double y_berri11 = y - (3 * sin(a*ROTAZIOA_MURRIZTU));
	double x_berri12 = x + (-3 * cos(a*ROTAZIOA_MURRIZTU));
	double y_berri12 = y - (-3 * sin(a*ROTAZIOA_MURRIZTU));
	double x_berri13 = x + (6 * cos(a*ROTAZIOA_MURRIZTU));
	double y_berri13 = y - (6 * sin(a*ROTAZIOA_MURRIZTU));
	double x_berri14 = x + (-6 * cos(a*ROTAZIOA_MURRIZTU));
	double y_berri14 = y - (-6 * sin(a*ROTAZIOA_MURRIZTU));
	double x_berri15 = x + (-6 * cos(a*ROTAZIOA_MURRIZTU) + 8 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri15 = y - (-6 * sin(a*ROTAZIOA_MURRIZTU) - 8 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri16 = x + (6 * cos(a*ROTAZIOA_MURRIZTU) + 8 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri16 = y - (6 * sin(a*ROTAZIOA_MURRIZTU) - 8 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri17 = x + (-3 * cos(a*ROTAZIOA_MURRIZTU) + 5 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri17 = y - (-3 * sin(a*ROTAZIOA_MURRIZTU) - 5 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri18 = x + (3 * cos(a*ROTAZIOA_MURRIZTU) + 5 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri18 = y - (3 * sin(a*ROTAZIOA_MURRIZTU) - 5 * cos(a*ROTAZIOA_MURRIZTU));

	
	zuzenaMarraztu((int)x_berri1, (int)y_berri1, (int)x_berri2, (int)y_berri2);
	zuzenaMarraztu((int)x_berri3, (int)y_berri3, (int)x_berri1, (int)y_berri1);
	zuzenaMarraztu((int)x_berri2, (int)y_berri2, (int)x_berri4, (int)y_berri4);
	zuzenaMarraztu((int)x_berri3, (int)y_berri3, (int)x_berri5, (int)y_berri5);
	zuzenaMarraztu((int)x_berri12, (int)y_berri12, (int)x_berri14, (int)y_berri14);
	zuzenaMarraztu((int)x_berri11, (int)y_berri11, (int)x_berri13, (int)y_berri13);
	
	zuzenaMarraztu((int)x_berri6, (int)y_berri6, (int)x_berri7, (int)y_berri7);
	zuzenaMarraztu((int)x_berri8, (int)y_berri8, (int)x_berri6, (int)y_berri6);

	zuzenaMarraztu((int)x_berri7, (int)y_berri7, (int)x_berri9, (int)y_berri9);
	zuzenaMarraztu((int)x_berri8, (int)y_berri8, (int)x_berri10, (int)y_berri10);
	
	arkatzKoloreaEzarri(4, 4, 252);
	zuzenaMarraztu((int)x_berri4, (int)y_berri4, (int)x_berri15, (int)y_berri15);
	zuzenaMarraztu((int)x_berri5, (int)y_berri5, (int)x_berri16, (int)y_berri16);
	zuzenaMarraztu((int)x_berri15, (int)y_berri15, (int)x_berri16, (int)y_berri16);
	zuzenaMarraztu((int)x_berri9, (int)y_berri9, (int)x_berri17, (int)y_berri17);
	zuzenaMarraztu((int)x_berri10,(int) y_berri10,(int) x_berri18, (int)y_berri18);
	zuzenaMarraztu((int)x_berri17, (int)y_berri17, (int)x_berri18, (int)y_berri18);

}