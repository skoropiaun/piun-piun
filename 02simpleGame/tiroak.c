#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "SDL_ttf.h"

#include "imagen.h"
#include "graphics.h"
#include"konstanteak.h"
#include "OurTypes.h"
#include "ontzia.h"
#include"tiroak.h"

//TIROA EGITEAN ONTZIAREN POSIZIOA ETA BERE ANGELUAREKIN, TIROAREN POSIZIOA ETA NORUNTZA KALKULATU DEZAKEGU.

//PARAMETROAK KALKULATU
void tiroEgin(double x, double y, double a) {
	tiro[datuak.tiroa].x1 = x + (-10 * sin(a*ROTAZIOA_MURRIZTU));
	tiro[datuak.tiroa].y1 = y - (10 * cos(a*ROTAZIOA_MURRIZTU));
	tiro[datuak.tiroa].x2 = x + (-20 * sin(a*ROTAZIOA_MURRIZTU));
	tiro[datuak.tiroa].y2 = y - (20 * cos(a*ROTAZIOA_MURRIZTU));
	tiro[datuak.tiroa].vx = 0 + (-10 * sin(a*ROTAZIOA_MURRIZTU));
	tiro[datuak.tiroa].vy = 3 - (10 * cos(a*ROTAZIOA_MURRIZTU));
}

//TIROA MARRAZTU ETA HONEN MUGIMENDUA ERE.

void tiroaMarraztu() {
	for (datuak.tiroa = 0; datuak.tiroa < datuak.zenbatTiro; datuak.tiroa++) {
		zuzenaMarraztu((int)tiro[datuak.tiroa].x1, (int)tiro[datuak.tiroa].y1, (int)tiro[datuak.tiroa].x2, (int)tiro[datuak.tiroa].y2);
		tiro[datuak.tiroa].x1 = tiro[datuak.tiroa].x1 + tiro[datuak.tiroa].vx;
		tiro[datuak.tiroa].y1 = tiro[datuak.tiroa].y1 + tiro[datuak.tiroa].vy;
		tiro[datuak.tiroa].x2 = tiro[datuak.tiroa].x2 + tiro[datuak.tiroa].vx;
		tiro[datuak.tiroa].y2 = tiro[datuak.tiroa].y2 + tiro[datuak.tiroa].vy;
	}
}
