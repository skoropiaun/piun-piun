# PIUN-PIUN
This report contains the process of the development of the first  POBL of informatic
engineering in Mondragon Unibertsitatea. To complete this Project the PIUN-PIUN
game has been created. This game is based on the classic arcade ASTEROID. 
The player controls a spaceship and must destroy or evade the asteroids near him.
The aim of the game is to get the highest score.
To execute this game, download the all files and execute sdlExamplesVcWS.sln on Visual Studio. 

## 1 JOKOA
Blog honetan PIUN-PIUN jokoaren gauzapen tekniko eta grafikoa azaltzen da. Hau burutzeko atal hauetan banatzen da:
* Pantailaren antolakuntza grafikoa.
* Ontziak.
* Asteroideak.
* Kolisioak eta tiroak.
* Pantailaren limiteak.

## 1.1 PANTAILAREN ANTOLAKUNTZA GRAFIKOA
Ontziak eta asteroideak marrazteko lehenik eta behin koordenatu kartesiarrak aztertuko dira. Koordenatu kartesiarren zentroa (0,0) puntua da.

Koordenatuetako zentro horretatik ardatz horizontala, x ardatza, eskuinera begiratuz balio positiboak dira. Aldiz, ezkerrera begiratuz, balio negatiboak dira. Y ardatzari erreparatuz, zentrotik beherako balioak negatiboak dira eta zentrotik gorakoak, aldiz, positiboak.

Egindako irudi guztiak marrazteko sistema hau hartu da erreferentzia gisa. Zentroa (0,0) izan beharrean jokoan (x,y) posizioa izango da. Izan ere jokoaren pantailari erreparatuz gero, (0,0) posizioa ezkerreko goiko ertza da. Hortik horizontalean eskuinera eta beherantz positiboa da.

## 1.2 ONTZIAK
Ontzia jokoaren funtsa izango da. Hau erabiltzaileak kontrolatuko du eta gezien bitartez manejatu. Honen helburua asteroideak apurtzen ahalik eta puntu gehien lortzea da. Hiru bizitza izango ditu, hau da hiru aldiz jo daiteke asteroide baten kontra.

## 1.2.1 ONTZIAK MARRAZTU
Aurretik azalduta dagoen bezala sistema kartesiarra erreferentzia bezala hartu da eta honen zentroa aldatu (x,y) puntua lortuz.

Ontzia marrazteko, 10x10 pixel dimentsiotako karratu baten barruan sortu dira irudiak. Hau burutzeko paper batean eskuz egin dira ideia guztiak. Ondoren puntuak kalkulatu eta programatu dira. Hona hemen onzti baten puntuen kalkuluak: 

```
        double x_berri1 = x + (-10 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri1 = y - (10 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri2 = x + (-6 * cos(a*ROTAZIOA_MURRIZTU) + 3 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri2 = y - (-6 * sin(a*ROTAZIOA_MURRIZTU) - 3 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri3 = x + (6 * cos(a*ROTAZIOA_MURRIZTU) + 3 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri3 = y - (6 * sin(a*ROTAZIOA_MURRIZTU) - 3 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri4 = x + (-4 * cos(a*ROTAZIOA_MURRIZTU) + 6 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri4 = y - (-4 * sin(a*ROTAZIOA_MURRIZTU) - 6 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri5 = x + (4 * cos(a*ROTAZIOA_MURRIZTU) + 6 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri5 = y - (4 * sin(a*ROTAZIOA_MURRIZTU) - 6 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri6 = x + (-6 * cos(a*ROTAZIOA_MURRIZTU) - 4 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri6 = y - (-6 * sin(a*ROTAZIOA_MURRIZTU) + 4 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri7 = x + (-6 * cos(a*ROTAZIOA_MURRIZTU) + 8 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri7 = y - (-6 * sin(a*ROTAZIOA_MURRIZTU) - 8 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri8 = x + (6 * cos(a*ROTAZIOA_MURRIZTU) - 4 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri8 = y - (6 * sin(a*ROTAZIOA_MURRIZTU) + 4 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri9 = x + (6 * cos(a*ROTAZIOA_MURRIZTU) + 8 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri9 = y - (6 * sin(a*ROTAZIOA_MURRIZTU) - 8 * cos(a*ROTAZIOA_MURRIZTU));
```

Puntuak programan sartu ondoren, beraien artean marrak marraztu dira lehenengo puntutik hasita eta berdinean amaituz.
```
        zuzenaMarraztu((int)x_berri1, (int)y_berri1, (int)x_berri2, (int)y_berri2);
	zuzenaMarraztu((int)x_berri3, (int)y_berri3, (int)x_berri1, (int)y_berri1);
	zuzenaMarraztu((int)x_berri3, (int)y_berri3, (int)x_berri5, (int)y_berri5);
	zuzenaMarraztu((int)x_berri1, (int)y_berri1, (int)x_berri2, (int)y_berri2);
	zuzenaMarraztu((int)x_berri2, (int)y_berri2, (int)x_berri4, (int)y_berri4);
	zuzenaMarraztu((int)x_berri4, (int)y_berri4, (int)x_berri5, (int)y_berri5);
	zuzenaMarraztu((int)x_berri6, (int)y_berri6, (int)x_berri7, (int)y_berri7);
	zuzenaMarraztu((int)x_berri8, (int)y_berri8, (int)x_berri9, (int)y_berri9);
```

## 1.2.2 ONTZIAREN MUGIMENDUA
Hau lortzeko propultsioa eta higidura zuzen uniformea erabili dira .Ontzia geldirik hasiko da hasierako unean eta (x,y) koordenatu batzuk izango ditu. Hau ontziaren zentroa izango da eta gainontzeko puntuak zentro honekiko x eta y ardatzean desberdintasun bat izango dute.

Ontziaren mugimendua esleitzerakoan berez, higidura zuzen uniformea esleitu da. Denbora tarte baten ondoren honek posizioaren aldaketa jasaten du. Horretarako vx eta vy erabili dira. Hauek (x,y) zentroari gehitzen zaizkio eta ontziaren puntu berriak kalkulatzen dira. Honen ondoren marraztu egiten dira.

Baina, honekin ez da nahikoa, espazioan ez dago abiadura konstante bat , ezta biraketa konstanterik ere ez. Beraz, propultsioa jarri zaio. Gorako gezia sakatzean aurrerantz joango da eta kontrakoa egingo du beheranzko gezia sakatzerakoan. 

Hau egiteko ax eta ay erabili dira, hauei azelerazioa edo propultsioa deitu zaie. Hauek botoia sakatzerakoan vx eta vy-ri gehitzen zaizkio. Horrela vx eta vy ren aldakuntza lortzen da eta  ontziaren zentroa mugitzen da.
```
        ontzia.x = ontzia.x + ontzia.vx;
		ontzia.y = ontzia.y + ontzia.vy;
		switch (ebentu) {
			case TECLA_RIGHT: ontzia.a = ontzia.a - 1;
				break;
			case TECLA_LEFT: ontzia.a = ontzia.a + 1;
				break;
			case TECLA_UP: ontzia.ax = (-0.25 * sin(ontzia.a*ROTAZIOA_MURRIZTU));
				ontzia.ay = -(0.25 * cos(ontzia.a*ROTAZIOA_MURRIZTU));
				ontzia.vx = ontzia.vx + ontzia.ax;
				ontzia.vy = ontzia.vy + ontzia.ay;
				break;
			case TECLA_DOWN: ontzia.ax = (-0.25 * sin(ontzia.a*ROTAZIOA_MURRIZTU));
				ontzia.ay = -(0.25 * cos(ontzia.a*ROTAZIOA_MURRIZTU));
				ontzia.vx = ontzia.vx - ontzia.ax;
				ontzia.vy = ontzia.vy - ontzia.ay;
				break;
		}
```

## 1.2.3 ONTZIAREN ERROTAZIOA
Biraketa edo rotazioa emateko rotazio indize bat sortu da, A. Eskuineko edo ezkerreko gezi botoia sakatzerakoan A+1 edo A-1 egiten da. Rotazioaren balioa lortu eta gero puntu berriak kalkulatzen dira. (x,y) zentroa izanik eta rotazio indizea sortuta ondorengo formulak erabiliz puntu berriak lortu dira.  x^'=x*cosα-y*sinα eta  y^'=x*sinα+y*cosα

Puntu berriak kalkulatzeko θ eta φ angeluen arteko batuketa egiten da. Hortik hurrengo formulak lortuz:

x=OA=r*cos⁡(θ+ϕ)     x^'=OA^'=r*cos⁡(ϕ
)
y=AP=r*sin⁡〖 (θ+ϕ)〗      y^'=A^' P=r*sin⁡(ϕ)

Formula hauek garatuz eta beraien artean sistema eginez honelako hau lortzen dugu:

x=r*cosθ*cosϕ-r*sinθ*sinϕ=x^'*cosθ-y^'*sinθ

y=r*sinθ*cosϕ+r*cosθ*sinϕ=x^'*sinθ+y^'*cosθ

## 1.3 ASTEROIDEAK
Asteroideak jokoan zehar gehien ikusten diren elementuak dira. Bakoitzak forma ezberdina dauka formula aleatorio batekin sortuta baitaude. Lau asteroide mota daude. Asteroideak apurtzean puntuak lortuko ditu erabiltzaileak, 18, 30, 54 eta 90 hauen tipoaren arabera .Apurtutako asteroide bakoitzeko bi asteroide txikiago ateratzen dira. 450 punturo asteroide handi bat sortuko da.

## 1.3.1ASTEROIDEAK MARRAZTU
Asteroideak marrazteko zentro bezala (x,y) puntu aleatorio bat hartu eta sistema kartesiarraren arabera zirkulu bat marrazten da. Asteroidearen forma zirkularra hobetzeko 12 puntu kalkulatze dira honen erradioaren arabera, begiratu 4-7 irudia. Puntu hauek beraien artean 30 graduko desfasea daukate. Puntu berri horiek lortzeko asteroidearen erradioa erreferentzia bezala hartzen dira. Erradioko puntu horretatik ausazko distantziara dagoen (x,y) puntua kalkulatzen da ondorengo funtzioak aplikatuz:

x^'=x+(-aleatorioa*sin⁡(30*puntuaren znbakia))

y=y-(aleatorioa*cos⁡(30*puntuaren znbakia))
 
Puntu hauek kalkulatu ondoren marren bitartez lotzen dira, lehenengotik hasita puntu berdin horretan amaituz.

Asteroide bat marrazteko adibidea:

```
void asteroideaMarraztu() {
int i = 0, y_berri = 0, j = 0;
	for (i = 0; i < datuak.zenbatPantailan; i++) {
		switch (asteroide[i].tipo) {
		case 0: arkatzKoloreaEzarri(245, 100, 150);
			break;
		case 1: arkatzKoloreaEzarri(190, 190, 150);
			break;
		case 2: arkatzKoloreaEzarri(0, 255, 0);
			break;
		case 3: arkatzKoloreaEzarri(255, 0, 0);
			break;
		}
		//ASTEROIDEAK SORTZEKO LINEAK MARRAZTU
		for (j = 0; j < 11; j++) {
			zuzenaMarraztu((int)asteroide[i].puntua[j].x, (int)asteroide[i].puntua[j].y, (int)asteroide[i].puntua[j + 1].x, (int)asteroide[i].puntua[j + 1].y);
		}
		zuzenaMarraztu((int)asteroide[i].puntua[11].x, (int)asteroide[i].puntua[11].y, (int)asteroide[i].puntua[0].x, (int)asteroide[i].puntua[0].y);

	}

}
```


## 1.3.2 ASTEROIDEEN MUGIMENDUA
Asteroidea mugitzeko, lehenik ausazko abiadura bat kalkulatzen da, V. Pb = Ph + V ekuazioa aplikatuz puntu bakoitza V pixel mugituko da. Hau denboraren araberako da eta mugimendua konstantea da. Mugimendua egiteko, puntu berria kalkulatzean pantailan marrazten da eta aurreko irudia ezabatzen da.

Asteroideen mugimendua bi ardatzetan banatzen da, beraz ekuazioa bi aldiz erabiltzen da. Ekuazioa interpretatuz asteroidearen amaierako posizioa hasierako posizioaren eta ardatzeko abiaduraren arteko batura izango da.

```
void asteroideaMugitu() {
int j = 0;
	for (int i = 0; i < datuak.zenbatPantailan; i++) {
		asteroide[i].x = asteroide[i].x + asteroide[i].vx;
		asteroide[i].y = asteroide[i].y + asteroide[i].vy;
		for (j = 0; j < 12; j++) {
			asteroide[i].puntua[j].x = asteroide[i].puntua[j].x + asteroide[i].vx;
			asteroide[i].puntua[j].y = asteroide[i].puntua[j].y + asteroide[i].vy;
		}
		//  X ARDATZEAN ALDE BATETIK BESTERA
		if (asteroide[i].x > 633) {
			asteroide[i].x = asteroide[i].x + asteroide[i].radio - 633;
			for (j = 0; j < 12; j++) {
				asteroide[i].puntua[j].x = asteroide[i].puntua[j].x + asteroide[i].radio - 633;
			}
		}
		else if (asteroide[i].x < 7) {
			asteroide[i].x = asteroide[i].x - asteroide[i].radio + 633;
			for (j = 0; j < 12; j++) {
				asteroide[i].puntua[j].x = asteroide[i].puntua[j].x - asteroide[i].radio + 633;
			}
		}
		//  Y ARDATZEAN ALDE BATETIK BESTERA
		if (asteroide[i].y > 480) {
			asteroide[i].y = asteroide[i].y + asteroide[i].radio - 450;
			for (j = 0; j < 12; j++) {
				asteroide[i].puntua[j].y = asteroide[i].puntua[j].y + asteroide[i].radio - 450;
			}
		}
		else if (asteroide[i].y < 30) {
			asteroide[i].y = asteroide[i].y - asteroide[i].radio + 450;
			for (j = 0; j < 12; j++) {
				asteroide[i].puntua[j].y = asteroide[i].puntua[j].y - asteroide[i].radio + 450;
			}
		}
	}
}
```


## 1.4 TIROAK ETA KOLISIOAK
Tiroak espazioa sakatzerakoan gure espazio ontziaren puntatik aterako dira., honek seinalatzen duen norabidean. Tiroak marra zuzenak izango dira. Hauek asteroideekin topatzean asteroidea suntsitzen dute tiroa desagerraraziz. 

Asteroidearen eta tiro edo ontziaren arteko kolisioa gertatu dela jakiteko beraien puntuen eta asteroidearen zentroaren arteko distantzia kalkulatzen da. Lortutako zenbaki hori asteroidearen erradioarekin konparatzen da. Asteroidearen erradioa baino txikiagoa bada kolisioa gertatu da. Hurrengo formula erabiltzen da:

√(((x-x_asteroide )^2+) (y-y_asteroide )^2)<asteroidearen radioa

```

//ONTZIAK ASTEROIDEA IKUTZEN BADU
int barruanDago(int i, double x, double y) {
	double dx = x - asteroide[i].x, dy = y - asteroide[i].y;
	return sqrt(dx*dx + (dy*dy)) < asteroide[i].radio;
}
//TIROA ASTEROIDEA IKUTZEN BADU
int kolisioaTiroak(int k, int z) {
	double dx = tiro[k].x2 - asteroide[z].x, dy = tiro[k].y2 - asteroide[z].y;
	return sqrt(dx*dx + (dy*dy)) <= asteroide[z].radio;
}
//TIROAK ASTEROIDEA EMATEAN ASTEROIDE HORI EZABATZEKO.
void elementuakEzabatu(int k, int z) {
	
	int tipotmp = asteroide[z].tipo;
	int tmpx = asteroide[z].x;
	int tmpy = asteroide[z].y;
	puntuatu(tipotmp);

	for (int i = k; i < datuak.zenbatTiro; i++) {
		tiro[i].x1 = tiro[i + 1].x1;
		tiro[i].y1 = tiro[i + 1].y1;
		tiro[i].x2 = tiro[i + 1].x2;
		tiro[i].y2 = tiro[i + 1].y2;
	}
	datuak.zenbatTiro--;
	for (int h = z; h < datuak.zenbatPantailan; h++) {

		asteroide[h].x = asteroide[h + 1].x;
		asteroide[h].y = asteroide[h + 1].y;
		asteroide[h].radio = asteroide[h + 1].radio;
		asteroide[h].vx = asteroide[h + 1].vx;
		asteroide[h].vy = asteroide[h + 1].vy;
		asteroide[h].tipo = asteroide[h + 1].tipo;
		for (int j = 0; j < 12; j++) {
			asteroide[h].puntua[j].y = asteroide[h + 1].puntua[j].y;
			asteroide[h].puntua[j].x = asteroide[h + 1].puntua[j].x;
		}
	}
	asteroideBerriaKalkulatu(tipotmp, tmpx, tmpy);
	datuak.zenbatPantailan++;
	asteroideBerriaKalkulatu(tipotmp, tmpx, tmpy);
}
// BZIZITZAK ZENBAT PANTAILARATU BEHAR DIREN
void bizitza(int id) {
	switch (datuak.bizitzak) {
		case 1:irudiaMugitu(id, 600, 10);
			irudiakMarraztu();
			break;
		case 2:irudiaMugitu(id, 600, 10);
			irudiakMarraztu();
			irudiaMugitu(id, 570, 10);
			irudiakMarraztu();
			break;
		case 3: irudiaMugitu(id, 600, 10);
			irudiakMarraztu();
			irudiaMugitu(id, 570, 10);
			irudiakMarraztu();
			irudiaMugitu(id, 540, 10);
			irudiakMarraztu();
			break;
	}
}
// APURTUTAKO ASTEROIDEAREN ARABERA ZENBAT PUNTU IRABAZIKO DITUZUN
void puntuatu(int tipo) {
	switch (tipo) {
	case 0: datuak.score = datuak.score + 90;
		break;
	case 1: datuak.score = datuak.score + 54;
		break;
	case 2: datuak.score = datuak.score + 30;
		break;
	case 3: datuak.score = datuak.score + 18;
		break;
	}

}
// ONTZIAREN ARABERA ZEIN KOLISIO KARGATU BEHAR DEN JAKITEKO

int kolisioa(int i, double a, double x, double y) {
	int erantzuna = 0;


	switch (ontzia.zenbakia) {

	case 0: erantzuna = kolisioaSTARWARS(i, a, x, y);
		break;
	case 1: erantzuna = kolisioaKOROA(i, a, x, y);
		break;
	case 2: erantzuna = kolisioaFIGURA(i, a, x, y);
		break;
	case 3: erantzuna = kolisioaMU(i, a, x, y);
		break;
	case 4: erantzuna = kolisioaPERTSONAIA(i, a, x, y);
		break;
	case 5: erantzuna = kolisioaSINPLE(i, a, x, y);
		break;
	}
	return erantzuna;
}

//ONTZI BAKOITZAREN KOLISIOAK

int kolisioaSINPLE(int i, double a, double x, double y) {



	int tipotmp = asteroide[i].tipo;
	int tmpx = 1 + rand() % ((640 + 1) - 1);
	int tmpy = 1 + rand() % ((480 + 1) - 1);

	double x_berri1 = x + (-10 * sin(a*ROTAZIOA_MURRIZTU));
	double y_berri1 = y - (10 * cos(a*ROTAZIOA_MURRIZTU));
	double x_berri2 = x + (+7.07106781*cos(a*ROTAZIOA_MURRIZTU) + 7.07106781*sin(a*ROTAZIOA_MURRIZTU));
	double y_berri2 = y - (7.07106781*sin(a*ROTAZIOA_MURRIZTU) - 7.07106781*cos(a*ROTAZIOA_MURRIZTU));
	double x_berri3 = x + (-7.07106781*cos(a*ROTAZIOA_MURRIZTU) + 7.07106781*sin(a*ROTAZIOA_MURRIZTU));
	double y_berri3 = y - (-7.07106781*sin(a*ROTAZIOA_MURRIZTU) - 7.07106781*cos(a*ROTAZIOA_MURRIZTU));
	if (barruanDago(i, x_berri1, y_berri1) != 0 ||
		barruanDago(i, x_berri2, y_berri2) != 0 ||
		barruanDago(i, x_berri3, y_berri3) != 0 ||
		barruanDago(i, (x_berri1 + x_berri2) / 2, (y_berri1 + y_berri2) / 2) ||
		barruanDago(i, (x_berri2 + x_berri3) / 2, (y_berri2 + y_berri3) / 2) ||
		barruanDago(i, (x_berri3 + x_berri1) / 2, (y_berri3 + y_berri1) / 2))
	{
		datuak.bizitzak = datuak.bizitzak - 1;
		for (int h = i; h < datuak.zenbatPantailan; h++) {

			asteroide[h].x = asteroide[h + 1].x;
			asteroide[h].y = asteroide[h + 1].y;
			asteroide[h].radio = asteroide[h + 1].radio;
			asteroide[h].vx = asteroide[h + 1].vx;
			asteroide[h].vy = asteroide[h + 1].vy;
			asteroide[h].tipo = asteroide[h + 1].tipo;
			for (int j = 0; j < 12; j++) {
				asteroide[h].puntua[j].y = asteroide[h + 1].puntua[j].y;
				asteroide[h].puntua[j].x = asteroide[h + 1].puntua[j].x;
			}
		}
		asteroideBerriaKalkulatu(tipotmp, tmpx, tmpy);
		datuak.zenbatPantailan++;
		asteroideBerriaKalkulatu(tipotmp, tmpx, tmpy);

	}
	return datuak.bizitzak;
}
```


## 1.5 PANTAILAREN LIMITEAK
Jokoaren limiteak pantaila berak ezartzen ditu. Goiko posizio maximoa y=0 da eta behekoa aldiz y=480. Ezkerretik ikusita x=0 eta eskuinetik x=640. Alde batetik desagertzen dena beste aldean agertzen da.

Objektu bat , ontzia adibidez, alde batera heltzen denean, x=640 denean, beste aldean agertu behar da. Horretarako, -640 egiten zaio eta bararen x=0 da honen ondoren.

Asteroideen kasuan erdiko puntua ertzera heltzen denean beste aldera pasatzen da. Asteroidea 12 puntuz osatuta dago eta puntu hauek kontuan hartu behar dira marrazteko momentuan. Erdiko puntua erreferentziatzat hartzen da .Beraz, limiteak ondorengoak dira: pantailaren limiteak + asteroidearen erradioa, goiko eta ezkerreko aldeetan eta, limiteak -asteroidearen erradioa beheko eta eskuineko aldean.

```
        if (ontzia.x > 640) {
			ontzia.x = ontzia.x - 640;
		}
		else if (ontzia.x < 0) {
			ontzia.x = ontzia.x + 640;
		}
		if (ontzia.y > 480) {
			ontzia.y = ontzia.y - 480;
		}
		else if (ontzia.y < 0) {
			ontzia.y = ontzia.y + 480;
		}
		
		if (asteroide[i].x > 633) {
			asteroide[i].x = asteroide[i].x + asteroide[i].radio - 633;
			for (j = 0; j < 12; j++) {
				asteroide[i].puntua[j].x = asteroide[i].puntua[j].x + asteroide[i].radio - 633;
			}
		}
		else if (asteroide[i].x < 7) {
			asteroide[i].x = asteroide[i].x - asteroide[i].radio + 633;
			for (j = 0; j < 12; j++) {
				asteroide[i].puntua[j].x = asteroide[i].puntua[j].x - asteroide[i].radio + 633;
			}
		}
		//  Y ARDATZEAN ALDE BATETIK BESTERA
		if (asteroide[i].y > 480) {
			asteroide[i].y = asteroide[i].y + asteroide[i].radio - 450;
			for (j = 0; j < 12; j++) {
				asteroide[i].puntua[j].y = asteroide[i].puntua[j].y + asteroide[i].radio - 450;
			}
		}
		else if (asteroide[i].y < 30) {
			asteroide[i].y = asteroide[i].y - asteroide[i].radio + 450;
			for (j = 0; j < 12; j++) {
				asteroide[i].puntua[j].y = asteroide[i].puntua[j].y - asteroide[i].radio + 450;
			}
		}
```

		





